#!/usr/bin/env python

import ROOT
import sys

if len(sys.argv) < 4:
    sys.exit("Uage: finalEvtMC.py [file list] [data/MC] [selection] \n")

isMC = False
if sys.argv[2] == "MC":
    isMC = True

lista = open(sys.argv[1], "r");
chain = ROOT.TChain("nominal")

#read files
while True:
    fName = lista.readline()
    fName = fName[:-1]
    if fName == "": break
    chain.Add(fName)

lista.close()

nEntries = chain.GetEntries()
sys.stdout.write("chain has "+str(nEntries)+" events\n")

#Final sum of weights for MC normalization
sumWeights = 0.0
#main loop
for evt in range(nEntries):
    chain.GetEntry(evt)
    selection = getattr(chain, sys.argv[3])
    if selection == 1 and chain.el_isTight == 1:
        #weights, if MC
        w = 1.0
        if isMC:
            w = w * chain.weight_mc
            w = w * chain.weight_pileup
            w = w * chain.weight_KFactor

        sumWeights += w

sys.stderr.write(" Final sum weights: "+"{0:6.5e}".format(sumWeights)+"\n")

