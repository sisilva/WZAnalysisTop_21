import sys
import math
import ROOT

SysUpNames = [
	"EG_RESOLUTION_ALL__1up",
	"EG_SCALE_ALL__1up",
	"MET_SoftTrk_ScaleUp",
	"MET_SoftTrk_ResoPerp",
	"JET_GlobalReduction_JET_BJES_Response__1up",
	"JET_GlobalReduction_JET_EffectiveNP_1__1up",
	"JET_GlobalReduction_JET_EffectiveNP_2__1up",
	"JET_GlobalReduction_JET_EffectiveNP_3__1up",
	"JET_GlobalReduction_JET_EffectiveNP_4__1up",
	"JET_GlobalReduction_JET_EffectiveNP_5__1up",
	"JET_GlobalReduction_JET_EffectiveNP_6__1up",
	"JET_GlobalReduction_JET_EffectiveNP_7__1up",
	"JET_GlobalReduction_JET_EffectiveNP_8restTerm__1up",
	"JET_GlobalReduction_JET_EtaIntercalibration_Modelling__1up",
	"JET_GlobalReduction_JET_EtaIntercalibration_NonClosure_highE__1up",
	"JET_GlobalReduction_JET_EtaIntercalibration_NonClosure_negEta__1up",
	"JET_GlobalReduction_JET_EtaIntercalibration_NonClosure_posEta__1up",
	"JET_GlobalReduction_JET_EtaIntercalibration_TotalStat__1up",
	"JET_GlobalReduction_JET_Flavor_Composition__1up",
	"JET_GlobalReduction_JET_Flavor_Response__1up",
	"JET_GlobalReduction_JET_Pileup_OffsetMu__1up",
	"JET_GlobalReduction_JET_Pileup_OffsetNPV__1up",
	"JET_GlobalReduction_JET_Pileup_PtTerm__1up",
	"JET_GlobalReduction_JET_Pileup_RhoTopology__1up",
	"JET_GlobalReduction_JET_PunchThrough_MC16__1up",
	"JET_GlobalReduction_JET_SingleParticle_HighPt__1up",
	"JET_JER_SINGLE_NP__1up"
]

SysDownNames = [
	"EG_RESOLUTION_ALL__1down",
	"EG_SCALE_ALL__1down",
	"MET_SoftTrk_ScaleDown",
	"MET_SoftTrk_ResoPara",
	"JET_GlobalReduction_JET_BJES_Response__1down",
	"JET_GlobalReduction_JET_EffectiveNP_1__1down",
	"JET_GlobalReduction_JET_EffectiveNP_2__1down",
	"JET_GlobalReduction_JET_EffectiveNP_3__1down",
	"JET_GlobalReduction_JET_EffectiveNP_4__1down",
	"JET_GlobalReduction_JET_EffectiveNP_5__1down",
	"JET_GlobalReduction_JET_EffectiveNP_6__1down",
	"JET_GlobalReduction_JET_EffectiveNP_7__1down",
	"JET_GlobalReduction_JET_EffectiveNP_8restTerm__1down",
	"JET_GlobalReduction_JET_EtaIntercalibration_Modelling__1down",
	"JET_GlobalReduction_JET_EtaIntercalibration_NonClosure_highE__1down",
	"JET_GlobalReduction_JET_EtaIntercalibration_NonClosure_negEta__1down",
	"JET_GlobalReduction_JET_EtaIntercalibration_NonClosure_posEta__1down",
	"JET_GlobalReduction_JET_EtaIntercalibration_TotalStat__1down",
	"JET_GlobalReduction_JET_Flavor_Composition__1down",
	"JET_GlobalReduction_JET_Flavor_Response__1down",
	"JET_GlobalReduction_JET_Pileup_OffsetMu__1down",
	"JET_GlobalReduction_JET_Pileup_OffsetNPV__1down",
	"JET_GlobalReduction_JET_Pileup_PtTerm__1down",
	"JET_GlobalReduction_JET_Pileup_RhoTopology__1down",
	"JET_GlobalReduction_JET_PunchThrough_MC16__1down",
	"JET_GlobalReduction_JET_SingleParticle_HighPt__1down",
	"JET_JER_SINGLE_NP__1up"
]

SysNames = dict(zip(SysUpNames, SysDownNames))
AllSys = SysUpNames + SysDownNames

histsName = [
	"DPHI",
	"ETA",
	"MU"
]

if len(sys.argv) < 2: exit("Usage: drawSys.py [inc or high]\n")
if sys.argv[1] == "inc": histsName += ["MET", "MT", "PT"]
elif sys.argv[1] == "high": histsName += ["METLOG", "MTLOG", "PTLOG", "NJETS"]
else: exit("Usage: drawSys.py [inc or high]\n")

labels = [
	"#Delta#phi",
	"#eta^{e}",
	"#mu",
	"E_{T}^{miss} [GeV]",
	"m_{T} [GeV]",
	"p_{T}^{e} [GeV]"
]

if sys.argv[1] == "high": labels += ["Number of jets"]

grLabelsX = dict(zip(histsName, labels))


SysUp = {}
SysDown = {}
SysUpSum = {}
SysDownSum = {}
histNominal = {}
histsFile = {}
nominalFile = {}
systFile = {}
outGraph = {}
mCanvas = {}
totalSysUp = {} #Integrals of systematics
totalSysDown = {}
hTotalSysUp = ROOT.TH1F("sysUp", "total sys. up", 6, 0.5, 6.5)
hTotalSysDown = ROOT.TH1F("sysDown", "total sys. down", 6, 0.5, 6.5)

for hist in histsName:
	nominalFile[hist] = ROOT.TFile("nominal/h"+hist+".root", "r")
	histNominal[hist] = nominalFile[hist].Get(hist)
	SysUpSum[hist] = histNominal[hist]-histNominal[hist]
	SysDownSum[hist] = histNominal[hist]-histNominal[hist]
	SysUp[hist] = {}
	SysDown[hist] = {}
	systFile[hist] = {}
	totalSysUp[hist] = 0.0
	totalSysDown[hist] = 0.0
	nBinsX = histNominal[hist].GetNbinsX() + 1
	for cima,baixo  in list(SysNames.items()):
		systFile[hist][cima] = ROOT.TFile(cima+"/h"+hist+".root", "r")
		SysUp[hist][cima] = systFile[hist][cima].Get(hist)
		if cima == "JET_JER_SINGLE_NP__1up":
			SysDown[hist][baixo] = SysUp[hist][cima].Clone()
		else:
			systFile[hist][baixo] = ROOT.TFile(baixo+"/h"+hist+".root", "r")
			SysDown[hist][baixo] = systFile[hist][baixo].Get(hist)
		#Calculate the integrals and sum to total systs.
		diffIntUp = SysUp[hist][cima].Integral() - histNominal[hist].Integral()
		totalSysUp[hist] += (diffIntUp * diffIntUp)
		diffIntDown = SysDown[hist][baixo].Integral() - histNominal[hist].Integral()
		totalSysDown[hist] += (diffIntDown * diffIntDown)
		for b in range(1,nBinsX):
			contentUp = math.fabs(SysUp[hist][cima].GetBinContent(b) - histNominal[hist].GetBinContent(b))
			#if hist == "MU": print b, histNominal[hist].GetBinContent(b), contentUp
			SysUp[hist][cima].SetBinContent(b, contentUp)
			SysUp[hist][cima].SetBinError(b, 0.0)
			SysUpSum[hist].AddBinContent(b, contentUp * contentUp)
			contentDown = math.fabs(SysDown[hist][baixo].GetBinContent(b) - histNominal[hist].GetBinContent(b))
			SysDown[hist][baixo].SetBinContent(b, contentDown)
			SysDown[hist][baixo].SetBinError(b, 0.0)
			SysDownSum[hist].AddBinContent(b, contentDown * contentDown)

	totalSysUp[hist] = math.sqrt(totalSysUp[hist])
	totalSysDown[hist] = math.sqrt(totalSysDown[hist])
	totalSysStr = hist + ": " + str(histNominal[hist].Integral()) + " + " + str(totalSysUp[hist]) + " - " + str(totalSysDown[hist]) + "\n"
	print totalSysStr
	hTotalSysUp.Fill(hist, totalSysUp[hist])
	hTotalSysDown.Fill(hist, totalSysDown[hist])
	outGraph[hist] = ROOT.TGraphAsymmErrors(nBinsX) #TGraph to show the error bars
	for b in range(1,nBinsX):
		q = SysUpSum[hist].GetBinContent(b)
		SysUpSum[hist].SetBinContent(b, math.sqrt(q))
		SysUpSum[hist].SetBinError(b, 0.0)
		q = SysDownSum[hist].GetBinContent(b)
		SysDownSum[hist].SetBinContent(b, math.sqrt(q))
		SysDownSum[hist].SetBinError(b, 0.0)
		outGraph[hist].SetPoint(b, histNominal[hist].GetBinCenter(b), histNominal[hist].GetBinContent(b))
		outGraph[hist].SetPointError(b, 0., 0., SysDownSum[hist].GetBinContent(b), SysUpSum[hist].GetBinContent(b))
		outGraph[hist].GetXaxis().SetTitle(grLabelsX[hist])
		outGraph[hist].GetYaxis().SetTitle("entries")
		outGraph[hist].SetTitle("W#rightarrow e#nu, El and MET scale and resolution")

	mCanvas[hist]=ROOT.TCanvas(hist, hist, 800, 600)
	outGraph[hist].Draw("AP")
	mCanvas[hist].Print(hist+".png")

outFile = ROOT.TFile("plotsWithSys.root", "recreate")

hTotalSysUp.Write("totalSysUp")
hTotalSysDown.Write("totalSysDown")
for hist in histsName:
	histNominal[hist].Write(hist+"_nom")
	nominalFile[hist].Close()
	SysDownSum[hist].Write(hist+"_down")
	SysUpSum[hist].Write(hist+"_up")
	for cima,baixo  in list(SysNames.items()):
		SysUp[hist][cima].Write(hist+"_"+cima)
		if cima == "JET_JER_SINGLE_NP__1up": continue
		SysDown[hist][baixo].Write(hist+"_"+baixo)


outFile.Write()
outFile.Close()

sys.exit(0)
