#!/usr/bin/env python

import ROOT
import sys

f=ROOT.TFile(sys.argv[1])
h=f.Get("ElTABLE")
print "130, 200: ", h.GetBinContent(1), "+/- ", h.GetBinError(1)
print "200, 400: ", h.GetBinContent(2), "+/- ", h.GetBinError(2)
print "400, 600: ", h.GetBinContent(3), "+/- ", h.GetBinError(3)
print "600, 1000: ", h.GetBinContent(4), "+/- ", h.GetBinError(4)
print "1000, 1500: ", h.GetBinContent(5), "+/- ", h.GetBinError(5)
f.Close()
