#!/usr/bin/env python
import sys
import math
import ROOT

###############################################################################
# Use these pages for Jets:
# https://svnweb.cern.ch/trac/atlasoff/browser/Reconstruction/Jet/JetUncertainties/trunk/share/JES_2016/Moriond2017/JES2016_21NP.config
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JetUncertaintiesRel21Summer2018SmallR
###############################################################################

atlasStyle = ROOT.TStyle("ATLAS", "Atlas style")
icol = 0
font = 42
tsize = 0.05
# use plain black on white colors
atlasStyle.SetFrameBorderMode(icol);
atlasStyle.SetFrameFillColor(icol);
atlasStyle.SetCanvasBorderMode(icol);
atlasStyle.SetCanvasColor(icol);
atlasStyle.SetPadBorderMode(icol);
atlasStyle.SetPadColor(icol);
atlasStyle.SetStatColor(icol);
atlasStyle.SetPaperSize(20,26);
atlasStyle.SetPadTopMargin(0.05);
atlasStyle.SetPadRightMargin(0.05);
atlasStyle.SetPadBottomMargin(0.16);
atlasStyle.SetPadLeftMargin(0.16);
atlasStyle.SetTitleXOffset(1.4);
atlasStyle.SetTitleYOffset(1.4);
atlasStyle.SetTextFont(font);
atlasStyle.SetTextSize(tsize);
atlasStyle.SetLabelFont(font,"x");
atlasStyle.SetTitleFont(font,"x");
atlasStyle.SetLabelFont(font,"y");
atlasStyle.SetTitleFont(font,"y");
atlasStyle.SetLabelFont(font,"z");
atlasStyle.SetTitleFont(font,"z");
atlasStyle.SetLabelSize(tsize,"x");
atlasStyle.SetTitleSize(tsize,"x");
atlasStyle.SetLabelSize(tsize,"y");
atlasStyle.SetTitleSize(tsize,"y");
atlasStyle.SetLabelSize(tsize,"z");
atlasStyle.SetTitleSize(tsize,"z");
atlasStyle.SetMarkerStyle(20);
atlasStyle.SetMarkerSize(1.2);
atlasStyle.SetHistLineWidth(2);
atlasStyle.SetLineStyleString(2,"[12 12]");
atlasStyle.SetEndErrorSize(0.);
atlasStyle.SetOptTitle(0);
atlasStyle.SetOptStat(0);
atlasStyle.SetOptFit(0);
atlasStyle.SetPadTickX(1);
atlasStyle.SetPadTickY(1);

ROOT.gROOT.SetStyle("ATLAS")
ROOT.gROOT.ForceStyle()

SysUpNamesMET = ["MET_SoftTrk_ScaleUp", "MET_SoftTrk_ResoPerp"]

SysUpNamesEG = ["EG_RESOLUTION_ALL__1up", "EG_SCALE_ALL__1up"]

SysUpNamesJES = [
	"JET_GlobalReduction_JET_BJES_Response__1up",
	"JET_GlobalReduction_JET_EffectiveNP_1__1up",
	"JET_GlobalReduction_JET_EffectiveNP_2__1up",
	"JET_GlobalReduction_JET_EffectiveNP_3__1up",
	"JET_GlobalReduction_JET_EffectiveNP_4__1up",
	"JET_GlobalReduction_JET_EffectiveNP_5__1up",
	"JET_GlobalReduction_JET_EffectiveNP_6__1up",
	"JET_GlobalReduction_JET_EffectiveNP_7__1up",
	"JET_GlobalReduction_JET_EffectiveNP_8restTerm__1up",
	"JET_GlobalReduction_JET_EtaIntercalibration_Modelling__1up",
	"JET_GlobalReduction_JET_EtaIntercalibration_NonClosure_highE__1up",
	"JET_GlobalReduction_JET_EtaIntercalibration_NonClosure_negEta__1up",
	"JET_GlobalReduction_JET_EtaIntercalibration_NonClosure_posEta__1up",
	"JET_GlobalReduction_JET_EtaIntercalibration_TotalStat__1up",
	"JET_GlobalReduction_JET_Flavor_Composition__1up",
	"JET_GlobalReduction_JET_Flavor_Response__1up",
	"JET_GlobalReduction_JET_Pileup_OffsetMu__1up",
	"JET_GlobalReduction_JET_Pileup_OffsetNPV__1up",
	"JET_GlobalReduction_JET_Pileup_PtTerm__1up",
	"JET_GlobalReduction_JET_Pileup_RhoTopology__1up",
	"JET_GlobalReduction_JET_PunchThrough_MC16__1up",
	"JET_GlobalReduction_JET_SingleParticle_HighPt__1up"
]

SysDownNamesMET = ["MET_SoftTrk_ScaleDown", "MET_SoftTrk_ResoPara"]

SysDownNamesEG = ["EG_RESOLUTION_ALL__1down", "EG_SCALE_ALL__1down"]

SysDownNamesJES = [
	"JET_GlobalReduction_JET_BJES_Response__1down",
	"JET_GlobalReduction_JET_EffectiveNP_1__1down",
	"JET_GlobalReduction_JET_EffectiveNP_2__1down",
	"JET_GlobalReduction_JET_EffectiveNP_3__1down",
	"JET_GlobalReduction_JET_EffectiveNP_4__1down",
	"JET_GlobalReduction_JET_EffectiveNP_5__1down",
	"JET_GlobalReduction_JET_EffectiveNP_6__1down",
	"JET_GlobalReduction_JET_EffectiveNP_7__1down",
	"JET_GlobalReduction_JET_EffectiveNP_8restTerm__1down",
	"JET_GlobalReduction_JET_EtaIntercalibration_Modelling__1down",
	"JET_GlobalReduction_JET_EtaIntercalibration_NonClosure_highE__1down",
	"JET_GlobalReduction_JET_EtaIntercalibration_NonClosure_negEta__1down",
	"JET_GlobalReduction_JET_EtaIntercalibration_NonClosure_posEta__1down",
	"JET_GlobalReduction_JET_EtaIntercalibration_TotalStat__1down",
	"JET_GlobalReduction_JET_Flavor_Composition__1down",
	"JET_GlobalReduction_JET_Flavor_Response__1down",
	"JET_GlobalReduction_JET_Pileup_OffsetMu__1down",
	"JET_GlobalReduction_JET_Pileup_OffsetNPV__1down",
	"JET_GlobalReduction_JET_Pileup_PtTerm__1down",
	"JET_GlobalReduction_JET_Pileup_RhoTopology__1down",
	"JET_GlobalReduction_JET_PunchThrough_MC16__1down",
	"JET_GlobalReduction_JET_SingleParticle_HighPt__1down"
]

SysNameJER = "JET_JER_SINGLE_NP__1up"
SysNamesMET = dict(zip(SysUpNamesMET, SysDownNamesMET))
SysNamesEG = dict(zip(SysUpNamesEG, SysDownNamesEG))
SysNamesJES = dict(zip(SysUpNamesJES, SysDownNamesJES))

histsName = [
	"DPHI",
	"ETA",
	"MU"
]

if len(sys.argv) < 2: exit("Usage: drawSys.py [inc or high]\n")
if sys.argv[1] == "inc": histsName += ["MET", "MT", "PT"]
elif sys.argv[1] == "high": histsName += ["METLOG", "MTLOG", "PTLOG", "NJETS"]
else: exit("Usage: drawSys.py [inc or high]\n")

labels = [
	"#Delta#phi",
	"#eta^{e}",
	"#mu",
	"E_{T}^{miss} [GeV]",
	"m_{T} [GeV]",
	"p_{T}^{e} [GeV]"
]

if sys.argv[1] == "high": labels += ["Number of jets"]

grLabelsX = dict(zip(histsName, labels))

mCanvas = {}
nominal = {}
SysUp = {}
SysUpSum = {}
SysDown = {}
SysDownSum = {}
SysJESUp = {}
SysJESDown = {}
SysJER = {}
fPlots = ROOT.TFile("plotsWithSys.root", "r")
l = {}

for hist in histsName:
	SysUp[hist] = {}
	SysDown[hist] = {}
        nominal[hist] = fPlots.Get(hist + "_nom")
	SysUpSum[hist] = fPlots.Get(hist + "_up")
	SysDownSum[hist] = fPlots.Get(hist + "_down")
	SysUpSum[hist].Add(SysDownSum[hist])
	SysUpSum[hist].Divide(nominal[hist])
	SysUpSum[hist].SetStats(0)
	SysUpSum[hist].SetLineColor(1)
	SysUpSum[hist].GetXaxis().SetTitle(grLabelsX[hist])
	SysUpSum[hist].GetYaxis().SetTitle("Relative systematic variation")
	mCanvas[hist] = ROOT.TCanvas(hist, hist, 800, 600)
	l[hist] = ROOT.TLegend(0.6, 0.7, 0.9, 0.9)
	SysUpSum[hist].Draw("HIST")
	l[hist].AddEntry(SysUpSum[hist], "Total", "l")
	mColor = 2
	plotStyle = "SAME HIST"
	for cima,baixo  in list(SysNamesMET.items()):
		SysUp[hist][cima] = fPlots.Get(hist + "_" +cima)
		SysDown[hist][baixo] = fPlots.Get(hist + "_" + baixo)
		SysUp[hist][cima].Add(SysDown[hist][baixo])
                SysUp[hist][cima].Divide(nominal[hist])
		SysUp[hist][cima].SetLineColor(mColor)
		SysUp[hist][cima].SetStats(0)
		SysUp[hist][cima].GetXaxis().SetTitle(grLabelsX[hist])
		SysUp[hist][cima].Draw(plotStyle)
		lName = cima[:-2]
		if cima == "MET_SoftTrk_ResoPerp":
			lName = "E_{T}^{miss} resolution"
		else: lName = "E_{T}^{miss} scale"
		l[hist].AddEntry(SysUp[hist][cima], lName, "l")
		mColor += 1
	for cima,baixo  in list(SysNamesEG.items()):
		SysUp[hist][cima] = fPlots.Get(hist + "_" +cima)
		SysDown[hist][baixo] = fPlots.Get(hist + "_" + baixo)
		SysUp[hist][cima].Add(SysDown[hist][baixo])
                SysUp[hist][cima].Divide(nominal[hist])
		SysUp[hist][cima].SetLineColor(mColor)
		SysUp[hist][cima].SetStats(0)
		SysUp[hist][cima].GetXaxis().SetTitle(grLabelsX[hist])
		SysUp[hist][cima].Draw(plotStyle)
		lName = cima[:-2]
		if cima == "EG_RESOLUTION_ALL__1up":
			lName = "Electron energy resolution"
		else: lName = "Electron energy scale"
		l[hist].AddEntry(SysUp[hist][cima], lName, "l")
		mColor += 1
	#JES
	SysJESUp[hist] = nominal[hist] - nominal[hist]
	SysJESDown[hist] = nominal[hist] - nominal[hist]
	nBinsX = SysJESUp[hist].GetNbinsX() + 1
	for cima,baixo  in list(SysNamesJES.items()):
		SysUp[hist][cima] = fPlots.Get(hist + "_" +cima)
		SysDown[hist][baixo] = fPlots.Get(hist + "_" + baixo)
		for b in range(1,nBinsX):
			contentUp = SysUp[hist][cima].GetBinContent(b)
			contentDown = SysDown[hist][baixo].GetBinContent(b)
			SysJESUp[hist].AddBinContent(b, contentUp * contentUp)
			SysJESDown[hist].AddBinContent(b, contentDown * contentDown)
	for b in range(1,nBinsX):
		qUp = SysJESUp[hist].GetBinContent(b)
		qDown = SysJESDown[hist].GetBinContent(b)
		SysJESUp[hist].SetBinContent(b, math.sqrt(qUp))
		SysJESDown[hist].SetBinContent(b, math.sqrt(qDown))
		SysJESUp[hist].SetBinError(b, 0.0)
		SysJESDown[hist].SetBinError(b, 0.0)
	
	SysJESUp[hist].Add(SysJESDown[hist])
	SysJESUp[hist].Divide(nominal[hist])
	SysJESUp[hist].SetStats(0)
	SysJESUp[hist].SetLineColor(mColor)
	SysJESUp[hist].Draw(plotStyle)
	lName = "Jet energy scale"
	l[hist].AddEntry(SysJESUp[hist], lName, "l")	
	mColor += 1
	
	#JER
	SysJER[hist] = fPlots.Get(hist + "_" + SysNameJER)
	SysJER[hist].Scale(2.0)
	SysJER[hist].Divide(nominal[hist])
	SysJER[hist].SetStats(0)
	SysJER[hist].SetLineColor(mColor)
	SysJER[hist].Draw(plotStyle)
	lName = "Jet energy resolution"
	l[hist].AddEntry(SysJER[hist], lName, "l")
	mColor += 1

	l[hist].SetBorderSize(0)
	l[hist].Draw()

