#!/usr/bin/env python
"""
Simple script to get initial sum of weights
"""

import ROOT
import sys

if len(sys.argv) < 2:
    sys.exit("Usage: initialEvtMC.py [files]\n")

unskim = 0
chain = ROOT.TChain("sumWeights")

for fName in sys.argv[1:]:
    sys.stderr.write("Opening "+fName+"\n")
    chain.Add(fName)

nEntries = chain.GetEntries()
print "n entries: ", nEntries
for iEntry in range(nEntries):
    chain.GetEntry(iEntry)
    unskim = unskim + chain.totalEventsWeighted

sys.stdout.write("Initial events*MC ")
sys.stdout.write("{0:6.5e}".format(unskim)+"\n")
