#!/usr/bin/env python
import sys
# Set up ROOT and RootCore:
import ROOT
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

# Initialize the xAOD infrastructure: 
if (not ROOT.xAOD.Init().isSuccess()): 
	sys.exit("Failed xAOD.Init()")

chain = ROOT.TChain("CollectionTree")

#Read input files
Xsec = float(sys.argv[1])
for fName in sys.argv[3:]:
	chain.Add(fName)

tree = ROOT.xAOD.MakeTransientTree(chain)
nEntries = tree.GetEntries()
sys.stderr.write("Number of events: "+str(nEntries)+".\n")

#Book histograms
hMW = ROOT.TH1F("MW", "m_{W}", 100, 120, 6120)

for entry in xrange(nEntries):
	if (entry % 100) == 0:
		sys.stderr.write(str(entry)+" events processed.\n")
	tree.GetEntry(entry)
	truthParticles = tree.TruthParticles
	for particle in truthParticles:
		if particle.isW(): 
			mass = 0.001 * particle.p4().M()
			hMW.Fill(mass)

#Normalize ro Xsec
hMW.Scale(Xsec / hMW.Integral())

outFile = ROOT.TFile(sys.argv[2], "recreate")
hMW.Write()
outFile.Write()
outFile.Close()

ROOT.xAOD.ClearTransientTrees()
