#!/usr/bin/env python

import ROOT
import sys
import getopt
import numpy as np 
import array

sys.dont_write_bytecode = True #prevents the creation of *.pyc files

#Error message
def usage():
	print "Usage: ntuple2hists2.py [options]"
	print "options are:"
	print "--lumi=luminosity in pb-1"
	print "--dsid=dataset id"
	print "--region=inclusive or high"
	print "--mc=True if is simulation or False if is data"
	print "--flist=file listing the input ntuples"
	print "--tree=tree name"
	print "--charge=plus or minus or both"
	print "--mva=True if you want to apply mva cut"
	return

#Get initial sum of unskimmed MC weights
def initialEvtMC(fList):
	lista = open(plotCfg["--fList"], "r");
	unskim = 0
	chain = ROOT.TChain("sumWeights")
	while True:
		fName = lista.readline()
		fName = fName[:-1]
		if fName == "": break
		print "Getting sumWeights tree from ", fName
		chain.Add(fName)
	nEntries = chain.GetEntries()
	print "sumWeights has: ", nEntries, " entries."
	for iEntry in range(nEntries):
		chain.GetEntry(iEntry)
		unskim = unskim + chain.totalEventsWeighted
	sys.stdout.write("Initial MC weights ")
	sys.stdout.write("{0:6.5e}".format(unskim)+"\n")
	lista.close()
	return unskim

#Cross section table for each DSID
xSec = {
	361100:11500.9154,#inclusive leptonic samples start here
	361102:11500.9154,
	361103:8579.0011,
	361105:8579.0011,
	361106:1950.6321,
	361108:1950.6321,
	363355:4.345748,#diboson samples start here
	363356:2.18227464,
	363357:6.7948,
	363358:3.4329,
	363359:24.719,
	363360:24.728,
	363489:11.421,
	364250:1.2523,
	364253:4.5832,
	364254:12.501,
	364255:3.231,
	410011:44.152,#top samples start here
	410012:26.276,
	410013:35.845486,
	410014:35.824406,
	410470:729.770,
	301060:(1000 * 3.2059E-02),#sliced samples start here
	301061:(1000 * 5.0033E-03),
	301062:(1000 * 1.7546E-03),
	301063:(1000 * 3.1237E-04),
	301064:(1000 * 6.0788E-05),
	301065:(1000 * 1.7666E-05),
	301066:(1000 * 7.2889E-06),
	301067:(1000 * 2.5068E-06),
	301068:(1000 * 9.8621E-07),
	301069:(1000 * 4.2450E-07),
	301070:(1000 * 1.9462E-07),
	301071:(1000 * 9.3340E-08),
	301072:(1000 * 4.6256E-08),
	301073:(1000 * 2.3473E-08),
	301074:(1000 * 1.8447E-08),
	301075:(1000 * 5.0963E-09),
	301076:(1000 * 1.4305E-09),
	301077:(1000 * 4.0123E-10),
	301078:(1000 * 1.5342E-10),
	301080:(1000 * 2.2194E-02),
	301081:(1000 * 3.2849E-03),
	301082:(1000 * 1.0831E-03),
	301083:(1000 * 1.7540E-04),
	301084:(1000 * 3.0979E-05),
	301085:(1000 * 8.2863E-06),
	301086:(1000 * 3.1593E-06),
	301087:(1000 * 1.0027E-06),
	301088:(1000 * 3.6812E-07),
	301089:(1000 * 1.4946E-07),
	301090:(1000 * 6.5308E-08),
	301091:(1000 * 3.0167E-08),
	301092:(1000 * 1.4548E-08),
	301093:(1000 * 7.2586E-09),
	301094:(1000 * 5.6689E-09),
	301095:(1000 * 1.5973E-09),
	301096:(1000 * 4.7207E-10),
	301097:(1000 * 1.4279E-10),
	301098:(1000 * 6.1619E-11),
	301140:(1000 * 3.2059E-02),
	301141:(1000 * 5.0033E-03),
	301142:(1000 * 1.7545E-03),
	301143:(1000 * 3.1237E-04),
	301144:(1000 * 6.0788E-05),
	301145:(1000 * 1.7666E-05),
	301146:(1000 * 7.2888E-06),
	301147:(1000 * 2.5068E-06),
	301148:(1000 * 9.8621E-07),
	301149:(1000 * 4.2453E-07),
	301150:(1000 * 1.9462E-07),
	301151:(1000 * 9.3340E-08),
	301152:(1000 * 4.6256E-08),
	301153:(1000 * 2.3473E-08),
	301154:(1000 * 1.8447E-08),
	301155:(1000 * 5.0963E-09),
	301156:(1000 * 1.4305E-09),
	301157:(1000 * 4.0124E-10),
	301158:(1000 * 1.5342E-10),
	301160:(1000 * 2.2194E-02),
	301161:(1000 * 3.2849E-03),
	301162:(1000 * 1.0831E-03),
	301163:(1000 * 1.7540E-04),
	301164:(1000 * 3.0979E-05),
	301165:(1000 * 8.2863E-06),
	301166:(1000 * 3.1593E-06),
	301167:(1000 * 1.0027E-06),
	301168:(1000 * 3.6812E-07),
	301169:(1000 * 1.4946E-07),
	301170:(1000 * 6.5307E-08),
	301171:(1000 * 3.0167E-08),
	301172:(1000 * 1.4548E-08),
	301173:(1000 * 7.2586E-09),
	301174:(1000 * 5.6689E-09),
	301175:(1000 * 1.5973E-09),
	301176:(1000 * 4.7207E-10),
	301177:(1000 * 1.4279E-10),
	301178:(1000 * 6.1619E-11),
	301000:(1000 * 1.7477E-02),
	301001:(1000 * 2.9215E-03),
	301002:(1000 * 1.0819E-03),
	301003:(1000 * 1.9551E-04),
	301004:(1000 * 3.7402E-05),
	301005:(1000 * 1.0607E-05),
	301006:(1000 * 4.2585E-06),
	301007:(1000 * 1.4220E-06),
	301008:(1000 * 5.4526E-07),
	301009:(1000 * 2.2992E-07),
	301010:(1000 * 1.0387E-07),
	301011:(1000 * 4.9403E-08),
	301012:(1000 * 2.4454E-08),
	301013:(1000 * 1.2490E-08),
	301014:(1000 * 1.0031E-08),
	301015:(1000 * 2.9344E-09),
	301016:(1000 * 8.9767E-10),
	301017:(1000 * 2.8071E-10),
	301018:(1000 * 1.2648E-10),
	301040:(1000 * 1.7477E-02),
	301041:(1000 * 2.9215E-03),
	301042:(1000 * 1.0819E-03),
	301043:(1000 * 1.9550E-04),
	301044:(1000 * 3.7403E-05),
	301045:(1000 * 1.0608E-05),
	301046:(1000 * 4.2586E-06),
	301047:(1000 * 1.4220E-06),
	301048:(1000 * 5.4526E-07),
	301049:(1000 * 2.2992E-07),
	301050:(1000 * 1.0387E-07),
	301051:(1000 * 4.9404E-08),
	301052:(1000 * 2.4455E-08),
	301053:(1000 * 1.2489E-08),
	301054:(1000 * 1.0031E-08),
	301055:(1000 * 2.9344E-09),
	301056:(1000 * 8.9768E-10),
	301057:(1000 * 2.8072E-10),
	301058:(1000 * 1.2647E-10)
	}

#Just a PI definition
mPi = np.arccos(-1.0)

if len(sys.argv) < 8:
	usage()
	sys.exit(1)

#Command line options
try:
	opts, args = getopt.getopt(sys.argv[1:], "", ["lumi=", "dsid=", "region=", "mc=", "fList=", "tree=", "charge=", "mva="])
except getopt.GetoptError as err:
	print str(err)
	usage()
	sys.exit(1)

plotCfg=dict(opts)

#book histograms
ElBaseNames = [
	"CALOISO",
	"D0SIG",
	"DPHI",
	"ETA",
	"MET",
	"MT",
	"MU",
	"PT",
	"TRKISO",
	"Z0SINT",
]
if plotCfg["--region"] == "high":
	ElBaseNames = ElBaseNames + ["METLOG", "MTLOG", "PTLOG", "TABLE", "NJETS"]

h = {}
h["CALOISO"] = ROOT.TH1F("CALOISO", "Electron topoetcone20/p_{T}", 50, 0, 0.5)
h["D0SIG"] = ROOT.TH1F("D0SIG", "Electron d_{0} sig", 100, -5, 5)
h["DPHI"] = ROOT.TH1F("DPHI", "#Delta#phi", 40, 0, mPi)
h["ETA"] = ROOT.TH1F("ETA", "Electron #eta", 100, -2.5, 2.5)
h["TRKISO"] = ROOT.TH1F("TRKISO", "Electron ptvarcone20/p_{T}", 50, 0, 0.5)
h["Z0SINT"] = ROOT.TH1F("Z0SINT", "Electron #Delta z_{0}#sin{#theta}", 60, -0.3, 0.3)
h["MU"] = ROOT.TH1F("MU", "#mu", 60, 0, 60)
#Inclusive region binning
if plotCfg["--region"] == "inclusive":
	h["PT"] = ROOT.TH1F("PT", "Electron p_{T}", 50, 25, 100)
	h["MET"] = ROOT.TH1F("MET", "E_{T}^{miss}", 50, 25, 100)
	h["MT"] = ROOT.TH1F("MT", "m_{T}", 50, 50, 120)
	h["MW"] = ROOT.TH1F("MW", "born m_{W}", 100, 50, 120)
#High mass binning
elif plotCfg["--region"] == "high":
	h["MET"] = ROOT.TH1F("MET", "E_{T}^{miss}", 50, 65, 1000)
	h["MT"] = ROOT.TH1F("MT", "m_{T}", 50, 130, 1000)
	h["MW"] = ROOT.TH1F("MW", "born m_{W}", 100, 120, 5120)
	h["PT"] = ROOT.TH1F("PT", "Electron p_{T}", 50, 65, 1000)
	#Log binning histograms
	nBins = 41
	ptMin = np.log10(65.0)
	ptMax = np.log10(2500.0)
	ptBins = np.linspace(ptMin, ptMax, nBins)
	ptBins = np.power(10, ptBins)
	mtMin = np.log10(130.0)
	mtMax = np.log10(4000.0)
	mtBins = np.linspace(mtMin, mtMax, nBins)
	mtBins = np.power(10, mtBins)
	h["PTLOG"] = ROOT.TH1F("PTLOG", "Electron p_{T}", 40, ptBins)
	h["METLOG"] = ROOT.TH1F("METLOG", "E_{T}^{miss}", 40, ptBins)
	h["MTLOG"] = ROOT.TH1F("MTLOG", "m_{T}", 40, mtBins)
    #Number of JETS
	h["NJETS"] = ROOT.TH1F("NJETS", "Number of JETS", 8, -0.5, 7.5)
	#binning for table
	tableBins = np.array([130.0, 200.0, 400.0, 600.0, 1000.0, 1500.0])
	h["TABLE"] = ROOT.TH1F("TABLE", "Transverse mass table", 5, tableBins)

#MVA variables
if plotCfg["--mva"] in ["True", "true"]:
	MVA_reader = ROOT.TMVA.Reader("V")
	MVA_el_pt = array.array("f",[0.])
	MVA_met = array.array("f",[0.])
	MVA_mt = array.array("f",[0.])
	MVA_dphi = array.array("f",[0.])
	MVA_njets = array.array("f",[0.])

	MVA_reader.AddVariable("el_pt", MVA_el_pt)
	MVA_reader.AddVariable("met", MVA_met)
	MVA_reader.AddVariable("mt", MVA_mt)
	MVA_reader.AddVariable("dphi", MVA_dphi)
	MVA_reader.AddVariable("njets", MVA_njets)
	#MVA_reader.AddSpectator("njets", MVA_njets)

	MVA_reader.BookMVA("BDTD","/home/simao/Rel21/MVAconfig/weights/TMVAClassification_BDTD.weights.xml")

#Final sum of weights for MC normalization
sumWeights = 0.0

#Selection name
if plotCfg["--region"] == "high":
	SelectionName = "ElectronSelection"
elif plotCfg["--region"] == "inclusive":
	SelectionName = "InclusiveSelection"
else:
	usage()
	exit(2)

#read files
lista = open(plotCfg["--fList"], "r");
treeName = plotCfg["--tree"]
chain = ROOT.TChain(treeName)
while True:
	fName = lista.readline()
	fName = fName[:-1]
	if fName == "": break
	chain.Add(fName)
lista.close()

#main loop
nEntries = chain.GetEntries()
sys.stdout.write("chain has "+str(nEntries)+" events\n")
for evt in range(nEntries):
	chain.GetEntry(evt)
	testSelection = getattr(chain, SelectionName)
	if testSelection == 1:
		#if not (chain.el_isTight): continue *Not working!
		pTl = 0.001 * chain.el_pt[0]
		isIso = False
		if pTl < 60.0: isIso = chain.el_isIsoTightTrackOnly
		else: isIso = chain.el_isIsoHighPt
		if not isIso: continue
		if plotCfg["--charge"] == "plus" and chain.el_charge[0] < 0:
			continue
		if plotCfg["--charge"] == "minus" and chain.el_charge[0] > 0:
			continue
		caloIso = 0.001 * chain.el_topoetcone20[0] / pTl
		trkIso = 0.001 * chain.el_ptvarcone20[0] / pTl
		DeltaPhi = chain.met_phi - chain.el_phi[0]
		if DeltaPhi > mPi: DeltaPhi = DeltaPhi - 2 * mPi
		if DeltaPhi < -mPi: DeltaPhi = DeltaPhi + 2 * mPi
		if DeltaPhi < 0: DeltaPhi = - DeltaPhi
		MET = 0.001 * chain.met_met
		MT = np.sqrt(2 * pTl * MET * (1 - np.cos(DeltaPhi)))
		if MT < 50: continue
		#MVA stuff
		passouMVA=True
		if plotCfg["--mva"] in ("True", "true"):
			MVA_el_pt[0] = chain.el_pt[0]
			MVA_met[0] = chain.met_met 
			MVA_mt[0] = 1000.0 * MT
			MVA_dphi[0] = chain.met_phi - chain.el_phi[0] 
			MVA_njets[0] = chain.jet_pt.size()
			MVA_Value = MVA_reader.EvaluateMVA("BDTD")
			MVA_Error = MVA_reader.GetMVAError()
			passouMVA = MVA_Value > -0.2
		if not passouMVA: continue
		if plotCfg["--mc"] in ("True", "true"):
			WbornMass = 0.001 #* chain.WbornMass
		#weights, if MC
		w = 1.0
		if plotCfg["--mc"] in ("True", "true"):
			w = w * chain.weight_mc
			w = w * chain.weight_pileup
			w = w * chain.weight_KFactor
			sumWeights += w
			#SFs
			if treeName == "nominal":
				w = w * chain.weight_indiv_SF_EL_ID
				if (pTl < 60.0): w = w * chain.el_IsoTightTrackOnlySF
				else: w = w * chain.el_IsoHighPtSF
				w = w * chain.weight_indiv_SF_EL_Trigger
				w = w * chain.weight_indiv_SF_EL_Reco
		h["ETA"].Fill(chain.el_eta[0], w)
		h["PT"].Fill(pTl, w)
		h["MET"].Fill(MET, w)
		h["DPHI"].Fill(DeltaPhi, w)
		h["MT"].Fill(MT, w)
		if plotCfg["--region"] == "high":
			h["MTLOG"].Fill(MT, w)
			h["PTLOG"].Fill(pTl, w)
			h["METLOG"].Fill(MET, w)
			h["TABLE"].Fill(MT, w)
			h["NJETS"].Fill(chain.jet_pt.size(), w)
		h["CALOISO"].Fill(caloIso, w)
		h["TRKISO"].Fill(trkIso, w)
		h["D0SIG"].Fill(chain.el_d0sig[0], w)
		h["Z0SINT"].Fill(chain.el_delta_z0_sintheta[0], w)
		if plotCfg["--mc"] in ("True", "true"):
			h["MU"].Fill(chain.mu, w)
			h["MW"].Fill(WbornMass, w)
		else: h["MU"].Fill(chain.mu / 1.03, w)

sys.stderr.write(" Final sum weights: "+"{0:6.5e}".format(sumWeights)+"\n")
#Normalize to luminosity if is MC
if plotCfg["--mc"] in ("True", "true"):
	L = float(plotCfg["--lumi"])
	iSumWeights = initialEvtMC(plotCfg["--fList"])
	x = xSec[int(plotCfg["--dsid"])]
	scale = L * x * (sumWeights / iSumWeights)
	N = h["MW"].Integral()
	if N > 0: h["MW"].Scale(scale / N)
	for BaseName in ElBaseNames:
		N = h[BaseName].Integral()
		h[BaseName].Scale(scale / N)

#save histograms
f = {} 
if plotCfg["--mc"] in ("True", "true"):
	f["MW"] = ROOT.TFile("hElMW.root", "recreate")
	h["MW"].Write()
	f["MW"].Write()
	f["MW"].Close()
for BaseName in ElBaseNames:
	f[BaseName] = ROOT.TFile("h"+BaseName+".root", "recreate")
	h[BaseName].Write()
	f[BaseName].Write()
	f[BaseName].Close()

