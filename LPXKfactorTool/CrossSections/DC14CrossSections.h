#include <iostream>
#include <map>

double
getDC14CrossSection(unsigned int datasetID){

	std::map<unsigned int, double> xsecMap;

	// NC DY Samples //
	xsecMap[147406] = 1.9002;     // Zee
	xsecMap[147436] = 1.9002;     // ZeeExactly1LeptonFilter
	xsecMap[147446] = 1.9003;     // ZeeDiLeptonFilter

	xsecMap[147407] = 1.9002;     // Zmumu

	xsecMap[147408] = 1.9003;     // Ztautau


	xsecMap[203500] = 0.01747;              // DYee_120M180
	xsecMap[203501] = 0.0029202;            // DYee_180M250
	xsecMap[203502] = 0.0010818;            // DYee_250M400
	xsecMap[203503] = 0.00019538;           // DYee_400M600
	xsecMap[203504] = 0.000037394;          // DYee_600M800
	xsecMap[203505] = 0.000010604;          // DYee_800M1000
	xsecMap[203506] = 0.0000042578;         // DYee_1000M1250
	xsecMap[203507] = 0.0000014218;         // DYee_1250M1500
	xsecMap[203508] = 0.0000005452;         // DYee_1500M1750
	xsecMap[203509] = 0.0000002299;         // DYee_1750M2000
	xsecMap[203510] = 0.00000010387;        // DYee_2000M2250
	xsecMap[203511] = 0.000000049394;       // DYee_2250M2500
	xsecMap[203512] = 0.000000024452;       // DYee_2500M2750
	xsecMap[203513] = 0.000000012487;       // DYee_2750M3000
	xsecMap[203514] = 0.000000010027;       // DYee_3000M3500
	xsecMap[203515] = 0.0000000029341;      // DYee_3500M4000
	xsecMap[203516] = 0.00000000089759;     // DYee_4000M4500
	xsecMap[203517] = 0.00000000028069;     // DYee_4500M5000
	xsecMap[203518] = 0.00000000012643;     // DYee_5000M

	xsecMap[203519] = 0.01747;              // DYmumu_120M180
	xsecMap[203520] = 0.0029202;            // DYmumu_180M250
	xsecMap[203521] = 0.0010818;            // DYmumu_250M400
	xsecMap[203522] = 0.00019538;           // DYmumu_400M600
	xsecMap[203523] = 0.000037394;          // DYmumu_600M800
	xsecMap[203524] = 0.000010604;          // DYmumu_800M1000
	xsecMap[203525] = 0.0000042578;         // DYmumu_1000M1250
	xsecMap[203526] = 0.0000014218;         // DYmumu_1250M1500
	xsecMap[203527] = 0.0000005452;         // DYmumu_1500M1750
	xsecMap[203528] = 0.0000002299;         // DYmumu_1750M2000
	xsecMap[203529] = 0.00000010387;        // DYmumu_2000M2250
	xsecMap[203530] = 0.000000049394;       // DYmumu_2250M2500
	xsecMap[203531] = 0.000000024452;       // DYmumu_2500M2750
	xsecMap[203532] = 0.000000012487;       // DYmumu_2750M3000
	xsecMap[203533] = 0.000000010027;       // DYmumu_3000M3500
	xsecMap[203534] = 0.0000000029341;      // DYmumu_3500M4000
	xsecMap[203535] = 0.00000000089759;     // DYmumu_4000M4500
	xsecMap[203536] = 0.00000000028069;     // DYmumu_4500M5000
	xsecMap[203537] = 0.00000000012643;     // DYmumu_5000M

	xsecMap[203538] = 0.017493;             // DYtautau_120M180
	xsecMap[203539] = 0.0029215;            // DYtautau_180M250
	xsecMap[203540] = 0.0010825;            // DYtautau_250M400
	xsecMap[203541] = 0.00019548;           // DYtautau_400M600
	xsecMap[203542] = 0.00003739;           // DYtautau_600M800
	xsecMap[203543] = 0.0000106;            // DYtautau_800M1000
	xsecMap[203544] = 0.000004258;          // DYtautau_1000M1250
	xsecMap[203545] = 0.0000014218;         // DYtautau_1250M1500
	xsecMap[203546] = 0.00000054523;        // DYtautau_1500M1750
	xsecMap[203547] = 0.00000022993;        // DYtautau_1750M2000
	xsecMap[203548] = 0.00000010388;        // DYtautau_2000M2250
	xsecMap[203549] = 0.000000049393;       // DYtautau_2250M2500
	xsecMap[203550] = 0.000000024453;       // DYtautau_2500M2750
	xsecMap[203551] = 0.000000012488;       // DYtautau_2750M3000
	xsecMap[203552] = 0.000000010028;       // DYtautau_3000M3500
	xsecMap[203553] = 0.0000000029343;      // DYtautau_3500M4000
	xsecMap[203554] = 0.00000000089765;     // DYtautau_4000M4500
	xsecMap[203555] = 0.00000000028073;     // DYtautau_4500M5000
	xsecMap[203556] = 0.00000000012645;     // DYtautau_5000M



	// CC DY Samples //
	xsecMap[147400] = 11.302;     // Wplusenu
	xsecMap[147401] = 11.302;     // Wplusmunu
	xsecMap[147402] = 11.302;     // Wplustaunu

	xsecMap[147403] = 8.28;     // Wminenu
	xsecMap[147404] = 8.28;     // Wminmunu
	xsecMap[147405] = 8.28;     // Wmintaunu

	xsecMap[203557] = 0.032048;            // Wplusenu_120M180
	xsecMap[203558] = 0.0050014;           // Wplusenu_180M250
	xsecMap[203559] = 0.001754;            // Wplusenu_250M400
	xsecMap[203560] = 0.00031232;          // Wplusenu_400M600
	xsecMap[203561] = 0.000060795;         // Wplusenu_600M800
	xsecMap[203562] = 0.00001767;          // Wplusenu_800M1000
	xsecMap[203563] = 0.0000072907;        // Wplusenu_1000M1250
	xsecMap[203564] = 0.0000025077;        // Wplusenu_1250M1500
	xsecMap[203565] = 0.0000009865;        // Wplusenu_1500M1750
	xsecMap[203566] = 0.00000042466;       // Wplusenu_1750M2000
	xsecMap[203567] = 0.00000019469;       // Wplusenu_2000M2250
	xsecMap[203568] = 0.000000093375;      // Wplusenu_2250M2500
	xsecMap[203569] = 0.000000046274;      // Wplusenu_2500M2750
	xsecMap[203570] = 0.000000023485;      // Wplusenu_2750M3000
	xsecMap[203571] = 0.000000018455;      // Wplusenu_3000M3500
	xsecMap[203572] = 0.0000000050982;     // Wplusenu_3500M4000
	xsecMap[203573] = 0.000000001431;      // Wplusenu_4000M4500
	xsecMap[203574] = 0.0000000004014;     // Wplusenu_4500M5000
	xsecMap[203575] = 0.00000000015343;    // Wplusenu_5000M

	xsecMap[203576] = 0.022181;            // Wminenu_120M180
	xsecMap[203577] = 0.0032836;           // Wminenu_180M250
	xsecMap[203578] = 0.0010825;           // Wminenu_250M400
	xsecMap[203579] = 0.00017533;          // Wminenu_400M600
	xsecMap[203580] = 0.000030967;         // Wminenu_600M800
	xsecMap[203581] = 0.0000082834;        // Wminenu_800M1000
	xsecMap[203582] = 0.0000031582;        // Wminenu_1000M1250
	xsecMap[203583] = 0.0000010027;        // Wminenu_1250M1500
	xsecMap[203584] = 0.00000036804;       // Wminenu_1500M1750
	xsecMap[203585] = 0.00000014943;       // Wminenu_1750M2000
	xsecMap[203586] = 0.000000065294;      // Wminenu_2000M2250
	xsecMap[203587] = 0.00000003016;       // Wminenu_2250M2500
	xsecMap[203588] = 0.000000014545;      // Wminenu_2500M2750
	xsecMap[203589] = 0.0000000072576;     // Wminenu_2750M3000
	xsecMap[203590] = 0.0000000056679;     // Wminenu_3000M3500
	xsecMap[203591] = 0.0000000015972;     // Wminenu_3500M4000
	xsecMap[203592] = 0.000000000472;      // Wminenu_4000M4500
	xsecMap[203593] = 0.00000000014277;    // Wminenu_4500M5000
	xsecMap[203594] = 0.000000000061587;   // Wminenu_5000M


	xsecMap[203595] = 0.032048;            // Wplusmunu_120M180
	xsecMap[203596] = 0.0050014;           // Wplusmunu_180M250
	xsecMap[203597] = 0.001754;            // Wplusmunu_250M400
	xsecMap[203598] = 0.00031232;          // Wplusmunu_400M600
	xsecMap[203599] = 0.000060795;         // Wplusmunu_600M800
	xsecMap[203600] = 0.00001767;          // Wplusmunu_800M1000
	xsecMap[203601] = 0.0000072907;        // Wplusmunu_1000M1250
	xsecMap[203602] = 0.0000025077;        // Wplusmunu_1250M1500
	xsecMap[203603] = 0.0000009865;        // Wplusmunu_1500M1750
	xsecMap[203604] = 0.00000042466;       // Wplusmunu_1750M2000
	xsecMap[203605] = 0.00000019469;       // Wplusmunu_2000M2250
	xsecMap[203606] = 0.000000093375;      // Wplusmunu_2250M2500
	xsecMap[203607] = 0.000000046274;      // Wplusmunu_2500M2750
	xsecMap[203608] = 0.000000023485;      // Wplusmunu_2750M3000
	xsecMap[203609] = 0.000000018455;      // Wplusmunu_3000M3500
	xsecMap[203610] = 0.0000000050982;     // Wplusmunu_3500M4000
	xsecMap[203611] = 0.000000001431;      // Wplusmunu_4000M4500
	xsecMap[203612] = 0.0000000004014;     // Wplusmunu_4500M5000
	xsecMap[203613] = 0.00000000015343;    // Wplusmunu_5000M

	xsecMap[203614] = 0.022181;            // Wminmunu_120M180
	xsecMap[203615] = 0.0032836;           // Wminmunu_180M250
	xsecMap[203616] = 0.0010825;           // Wminmunu_250M400
	xsecMap[203617] = 0.00017533;          // Wminmunu_400M600
	xsecMap[203618] = 0.000030967;         // Wminmunu_600M800
	xsecMap[203619] = 0.0000082834;        // Wminmunu_800M1000
	xsecMap[203620] = 0.0000031582;        // Wminmunu_1000M1250
	xsecMap[203621] = 0.0000010027;        // Wminmunu_1250M1500
	xsecMap[203622] = 0.00000036804;       // Wminmunu_1500M1750
	xsecMap[203623] = 0.00000014943;       // Wminmunu_1750M2000
	xsecMap[203624] = 0.000000065294;      // Wminmunu_2000M2250
	xsecMap[203625] = 0.00000003016;       // Wminmunu_2250M2500
	xsecMap[203626] = 0.000000014545;      // Wminmunu_2500M2750
	xsecMap[203627] = 0.0000000072576;     // Wminmunu_2750M3000
	xsecMap[203628] = 0.0000000056679;     // Wminmunu_3000M3500
	xsecMap[203629] = 0.0000000015972;     // Wminmunu_3500M4000
	xsecMap[203630] = 0.000000000472;      // Wminmunu_4000M4500
	xsecMap[203631] = 0.00000000014277;    // Wminmunu_4500M5000
	xsecMap[203632] = 0.000000000061587;   // Wminmunu_5000M


	xsecMap[203633] = 0.032048;            // Wplustaunu_120M180
	xsecMap[203634] = 0.0050014;           // Wplustaunu_180M250
	xsecMap[203635] = 0.001754;            // Wplustaunu_250M400
	xsecMap[203636] = 0.00031232;          // Wplustaunu_400M600
	xsecMap[203637] = 0.000060795;         // Wplustaunu_600M800
	xsecMap[203638] = 0.00001767;          // Wplustaunu_800M1000
	xsecMap[203639] = 0.0000072907;        // Wplustaunu_1000M1250
	xsecMap[203640] = 0.0000025077;        // Wplustaunu_1250M1500
	xsecMap[203641] = 0.0000009865;        // Wplustaunu_1500M1750
	xsecMap[203642] = 0.00000042466;       // Wplustaunu_1750M2000
	xsecMap[203643] = 0.00000019469;       // Wplustaunu_2000M2250
	xsecMap[203644] = 0.000000093375;      // Wplustaunu_2250M2500
	xsecMap[203645] = 0.000000046274;      // Wplustaunu_2500M2750
	xsecMap[203646] = 0.000000023485;      // Wplustaunu_2750M3000
	xsecMap[203647] = 0.000000018455;      // Wplustaunu_3000M3500
	xsecMap[203648] = 0.0000000050982;     // Wplustaunu_3500M4000
	xsecMap[203649] = 0.000000001431;      // Wplustaunu_4000M4500
	xsecMap[203650] = 0.0000000004014;     // Wplustaunu_4500M5000
	xsecMap[203651] = 0.00000000015343;    // Wplustaunu_5000M

	xsecMap[203652] = 0.022181;            // Wmintaunu_120M180
	xsecMap[203653] = 0.0032836;           // Wmintaunu_180M250
	xsecMap[203654] = 0.0010825;           // Wmintaunu_250M400
	xsecMap[203655] = 0.00017533;          // Wmintaunu_400M600
	xsecMap[203656] = 0.000030967;         // Wmintaunu_600M800
	xsecMap[203657] = 0.0000082834;        // Wmintaunu_800M1000
	xsecMap[203658] = 0.0000031582;        // Wmintaunu_1000M1250
	xsecMap[203659] = 0.0000010027;        // Wmintaunu_1250M1500
	xsecMap[203660] = 0.00000036804;       // Wmintaunu_1500M1750
	xsecMap[203661] = 0.00000014943;       // Wmintaunu_1750M2000
	xsecMap[203662] = 0.000000065294;      // Wmintaunu_2000M2250
	xsecMap[203663] = 0.00000003016;       // Wmintaunu_2250M2500
	xsecMap[203664] = 0.000000014545;      // Wmintaunu_2500M2750
	xsecMap[203665] = 0.0000000072576;     // Wmintaunu_2750M3000
	xsecMap[203666] = 0.0000000056679;     // Wmintaunu_3000M3500
	xsecMap[203667] = 0.0000000015972;     // Wmintaunu_3500M4000
	xsecMap[203668] = 0.000000000472;      // Wmintaunu_4000M4500
	xsecMap[203669] = 0.00000000014277;    // Wmintaunu_4500M5000
	xsecMap[203670] = 0.000000000061587;   // Wmintaunu_5000M



	// Top Samples //
	xsecMap[110401] = 0.69584;             // ttbar_nonallhad
	xsecMap[110070] = 0.043755;            // singletop_tchan_lept_top
	xsecMap[110071] = 0.025778;            // singletop_tchan_lept_antitop
	xsecMap[110302] = 0.0033514;           // st_schan_lep
	xsecMap[110305] = 0.068459;            // st_Wtchan_incl



	// Signal Samples //
	xsecMap[182916] = 0.0000095648;     // ZPrime ee 2 TeV
	xsecMap[182917] = 0.00000081568;    // ZPrime ee 3 TeV
	xsecMap[182918] = 0.000000095215;   // ZPrime ee 4 TeV
	xsecMap[182919] = 0.00000001646;    // ZPrime ee 5 TeV

	xsecMap[182920] = 0.000009596;     // ZPrime mumu 2 TeV
	xsecMap[182921] = 0.00000081355;     // ZPrime mumu 3 TeV
	xsecMap[182922] = 0.00000009545;     // ZPrime mumu 4 TeV
	xsecMap[182923] = 0.000000016605;     // ZPrime mumu 5 TeV


	xsecMap[203671] = 0.0003738;     // // WPrime emutau 2 TeV
	xsecMap[158762] = 0.000037674;     // // WPrime emutau 3 TeV
	xsecMap[203672] = 0.0000054315;     // // WPrime emutau 4 TeV
	xsecMap[203673] = 0.0000011991;     // // WPrime emutau 5 TeV


	xsecMap[158838] = 0.0004383;     // Wimp D52 DM1 MS1000
	xsecMap[158839] = 0.000428;     // Wimp D52 DM100 MS1000
	xsecMap[158843] = 0.0000396;     // Wimp D52 DM1300 MS1000


	try{
		return xsecMap[datasetID];
	}catch(const std::out_of_range& oor){
		std::cout<<"LPXKfactorTool: Failed to find cross section for dataset ID " <<datasetID;
	}
	return 0.0;

}



double
getDC14FilterEfficiency(unsigned int datasetID){

	std::map<unsigned int, double> filterEffMap;

	// NC DY Samples //
	filterEffMap[147406] = 1.0;     // Zee
	filterEffMap[147436] = 0.32495; // ZeeExactly1LeptonFilter
	filterEffMap[147446] = 0.52931; // ZeeDiLeptonFilter

	filterEffMap[147407] = 1.0;     // Zmumu

	filterEffMap[147408] = 1.0;     // Ztautau


	filterEffMap[203500] = 1.0;          // DYee_120M180
	filterEffMap[203501] = 1.0;          // DYee_180M250
	filterEffMap[203502] = 1.0;          // DYee_250M400
	filterEffMap[203503] = 1.0;          // DYee_400M600
	filterEffMap[203504] = 1.0;          // DYee_600M800
	filterEffMap[203505] = 1.0;          // DYee_800M1000
	filterEffMap[203506] = 1.0;          // DYee_1000M1250
	filterEffMap[203507] = 1.0;          // DYee_1250M1500
	filterEffMap[203508] = 1.0;          // DYee_1500M1750
	filterEffMap[203509] = 1.0;          // DYee_1750M2000
	filterEffMap[203510] = 1.0;          // DYee_2000M2250
	filterEffMap[203511] = 1.0;          // DYee_2250M2500
	filterEffMap[203512] = 1.0;          // DYee_2500M2750
	filterEffMap[203513] = 1.0;          // DYee_2750M3000
	filterEffMap[203514] = 1.0;          // DYee_3000M3500
	filterEffMap[203515] = 1.0;          // DYee_3500M4000
	filterEffMap[203516] = 1.0;          // DYee_4000M4500
	filterEffMap[203517] = 1.0;          // DYee_4500M5000
	filterEffMap[203518] = 1.0;          // DYee_5000M

	filterEffMap[203519] = 1.0;          // DYmumu_120M180
	filterEffMap[203520] = 1.0;          // DYmumu_180M250
	filterEffMap[203521] = 1.0;          // DYmumu_250M400
	filterEffMap[203522] = 1.0;          // DYmumu_400M600
	filterEffMap[203523] = 1.0;          // DYmumu_600M800
	filterEffMap[203524] = 1.0;          // DYmumu_800M1000
	filterEffMap[203525] = 1.0;          // DYmumu_1000M1250
	filterEffMap[203526] = 1.0;          // DYmumu_1250M1500
	filterEffMap[203527] = 1.0;          // DYmumu_1500M1750
	filterEffMap[203528] = 1.0;          // DYmumu_1750M2000
	filterEffMap[203529] = 1.0;          // DYmumu_2000M2250
	filterEffMap[203530] = 1.0;          // DYmumu_2250M2500
	filterEffMap[203531] = 1.0;          // DYmumu_2500M2750
	filterEffMap[203532] = 1.0;          // DYmumu_2750M3000
	filterEffMap[203533] = 1.0;          // DYmumu_3000M3500
	filterEffMap[203534] = 1.0;          // DYmumu_3500M4000
	filterEffMap[203535] = 1.0;          // DYmumu_4000M4500
	filterEffMap[203536] = 1.0;          // DYmumu_4500M5000
	filterEffMap[203537] = 1.0;          // DYmumu_5000M

	filterEffMap[203538] = 1.0;          // DYtautau_120M180
	filterEffMap[203539] = 1.0;          // DYtautau_180M250
	filterEffMap[203540] = 1.0;          // DYtautau_250M400
	filterEffMap[203541] = 1.0;          // DYtautau_400M600
	filterEffMap[203542] = 1.0;          // DYtautau_600M800
	filterEffMap[203543] = 1.0;          // DYtautau_800M1000
	filterEffMap[203544] = 1.0;          // DYtautau_1000M1250
	filterEffMap[203545] = 1.0;          // DYtautau_1250M1500
	filterEffMap[203546] = 1.0;          // DYtautau_1500M1750
	filterEffMap[203547] = 1.0;          // DYtautau_1750M2000
	filterEffMap[203548] = 1.0;          // DYtautau_2000M2250
	filterEffMap[203549] = 1.0;          // DYtautau_2250M2500
	filterEffMap[203550] = 1.0;          // DYtautau_2500M2750
	filterEffMap[203551] = 1.0;          // DYtautau_2750M3000
	filterEffMap[203552] = 1.0;          // DYtautau_3000M3500
	filterEffMap[203553] = 1.0;          // DYtautau_3500M4000
	filterEffMap[203554] = 1.0;          // DYtautau_4000M4500
	filterEffMap[203555] = 1.0;          // DYtautau_4500M5000
	filterEffMap[203556] = 1.0;          // DYtautau_5000M



	// CC DY Samples //
	filterEffMap[147400] = 1.0;          // Wplusenu
	filterEffMap[147401] = 1.0;          // Wplusmunu
	filterEffMap[147402] = 1.0;          // Wplustaunu

	filterEffMap[147403] = 1.0;          // Wminenu
	filterEffMap[147404] = 1.0;          // Wminmunu
	filterEffMap[147405] = 1.0;          // Wmintaunu

	filterEffMap[203557] = 1.0;          // Wplusenu_120M180
	filterEffMap[203558] = 1.0;          // Wplusenu_180M250
	filterEffMap[203559] = 1.0;          // Wplusenu_250M400
	filterEffMap[203560] = 1.0;          // Wplusenu_400M600
	filterEffMap[203561] = 1.0;          // Wplusenu_600M800
	filterEffMap[203562] = 1.0;          // Wplusenu_800M1000
	filterEffMap[203563] = 1.0;          // Wplusenu_1000M1250
	filterEffMap[203564] = 1.0;          // Wplusenu_1250M1500
	filterEffMap[203565] = 1.0;          // Wplusenu_1500M1750
	filterEffMap[203566] = 1.0;          // Wplusenu_1750M2000
	filterEffMap[203567] = 1.0;          // Wplusenu_2000M2250
	filterEffMap[203568] = 1.0;          // Wplusenu_2250M2500
	filterEffMap[203569] = 1.0;          // Wplusenu_2500M2750
	filterEffMap[203570] = 1.0;          // Wplusenu_2750M3000
	filterEffMap[203571] = 1.0;          // Wplusenu_3000M3500
	filterEffMap[203572] = 1.0;          // Wplusenu_3500M4000
	filterEffMap[203573] = 1.0;          // Wplusenu_4000M4500
	filterEffMap[203574] = 1.0;          // Wplusenu_4500M5000
	filterEffMap[203575] = 1.0;          // Wplusenu_5000M

	filterEffMap[203576] = 1.0;          // Wminenu_120M180
	filterEffMap[203577] = 1.0;          // Wminenu_180M250
	filterEffMap[203578] = 1.0;          // Wminenu_250M400
	filterEffMap[203579] = 1.0;          // Wminenu_400M600
	filterEffMap[203580] = 1.0;          // Wminenu_600M800
	filterEffMap[203581] = 1.0;          // Wminenu_800M1000
	filterEffMap[203582] = 1.0;          // Wminenu_1000M1250
	filterEffMap[203583] = 1.0;          // Wminenu_1250M1500
	filterEffMap[203584] = 1.0;          // Wminenu_1500M1750
	filterEffMap[203585] = 1.0;          // Wminenu_1750M2000
	filterEffMap[203586] = 1.0;          // Wminenu_2000M2250
	filterEffMap[203587] = 1.0;          // Wminenu_2250M2500
	filterEffMap[203588] = 1.0;          // Wminenu_2500M2750
	filterEffMap[203589] = 1.0;          // Wminenu_2750M3000
	filterEffMap[203590] = 1.0;          // Wminenu_3000M3500
	filterEffMap[203591] = 1.0;          // Wminenu_3500M4000
	filterEffMap[203592] = 1.0;          // Wminenu_4000M4500
	filterEffMap[203593] = 1.0;          // Wminenu_4500M5000
	filterEffMap[203594] = 1.0;          // Wminenu_5000M


	filterEffMap[203595] = 1.0;          // Wplusmunu_120M180
	filterEffMap[203596] = 1.0;          // Wplusmunu_180M250
	filterEffMap[203597] = 1.0;          // Wplusmunu_250M400
	filterEffMap[203598] = 1.0;          // Wplusmunu_400M600
	filterEffMap[203599] = 1.0;          // Wplusmunu_600M800
	filterEffMap[203600] = 1.0;          // Wplusmunu_800M1000
	filterEffMap[203601] = 1.0;          // Wplusmunu_1000M1250
	filterEffMap[203602] = 1.0;          // Wplusmunu_1250M1500
	filterEffMap[203603] = 1.0;          // Wplusmunu_1500M1750
	filterEffMap[203604] = 1.0;          // Wplusmunu_1750M2000
	filterEffMap[203605] = 1.0;          // Wplusmunu_2000M2250
	filterEffMap[203606] = 1.0;          // Wplusmunu_2250M2500
	filterEffMap[203607] = 1.0;          // Wplusmunu_2500M2750
	filterEffMap[203608] = 1.0;          // Wplusmunu_2750M3000
	filterEffMap[203609] = 1.0;          // Wplusmunu_3000M3500
	filterEffMap[203610] = 1.0;          // Wplusmunu_3500M4000
	filterEffMap[203611] = 1.0;          // Wplusmunu_4000M4500
	filterEffMap[203612] = 1.0;          // Wplusmunu_4500M5000
	filterEffMap[203613] = 1.0;          // Wplusmunu_5000M

	filterEffMap[203614] = 1.0;          // Wminmunu_120M180
	filterEffMap[203615] = 1.0;          // Wminmunu_180M250
	filterEffMap[203616] = 1.0;          // Wminmunu_250M400
	filterEffMap[203617] = 1.0;          // Wminmunu_400M600
	filterEffMap[203618] = 1.0;          // Wminmunu_600M800
	filterEffMap[203619] = 1.0;          // Wminmunu_800M1000
	filterEffMap[203620] = 1.0;          // Wminmunu_1000M1250
	filterEffMap[203621] = 1.0;          // Wminmunu_1250M1500
	filterEffMap[203622] = 1.0;          // Wminmunu_1500M1750
	filterEffMap[203623] = 1.0;          // Wminmunu_1750M2000
	filterEffMap[203624] = 1.0;          // Wminmunu_2000M2250
	filterEffMap[203625] = 1.0;          // Wminmunu_2250M2500
	filterEffMap[203626] = 1.0;          // Wminmunu_2500M2750
	filterEffMap[203627] = 1.0;          // Wminmunu_2750M3000
	filterEffMap[203628] = 1.0;          // Wminmunu_3000M3500
	filterEffMap[203629] = 1.0;          // Wminmunu_3500M4000
	filterEffMap[203630] = 1.0;          // Wminmunu_4000M4500
	filterEffMap[203631] = 1.0;          // Wminmunu_4500M5000
	filterEffMap[203632] = 1.0;          // Wminmunu_5000M


	filterEffMap[203633] = 1.0;          // Wplustaunu_120M180
	filterEffMap[203634] = 1.0;          // Wplustaunu_180M250
	filterEffMap[203635] = 1.0;          // Wplustaunu_250M400
	filterEffMap[203636] = 1.0;          // Wplustaunu_400M600
	filterEffMap[203637] = 1.0;          // Wplustaunu_600M800
	filterEffMap[203638] = 1.0;          // Wplustaunu_800M1000
	filterEffMap[203639] = 1.0;          // Wplustaunu_1000M1250
	filterEffMap[203640] = 1.0;          // Wplustaunu_1250M1500
	filterEffMap[203641] = 1.0;          // Wplustaunu_1500M1750
	filterEffMap[203642] = 1.0;          // Wplustaunu_1750M2000
	filterEffMap[203643] = 1.0;          // Wplustaunu_2000M2250
	filterEffMap[203644] = 1.0;          // Wplustaunu_2250M2500
	filterEffMap[203645] = 1.0;          // Wplustaunu_2500M2750
	filterEffMap[203646] = 1.0;          // Wplustaunu_2750M3000
	filterEffMap[203647] = 1.0;          // Wplustaunu_3000M3500
	filterEffMap[203648] = 1.0;          // Wplustaunu_3500M4000
	filterEffMap[203649] = 1.0;          // Wplustaunu_4000M4500
	filterEffMap[203650] = 1.0;          // Wplustaunu_4500M5000
	filterEffMap[203651] = 1.0;          // Wplustaunu_5000M

	filterEffMap[203652] = 1.0;          // Wmintaunu_120M180
	filterEffMap[203653] = 1.0;          // Wmintaunu_180M250
	filterEffMap[203654] = 1.0;          // Wmintaunu_250M400
	filterEffMap[203655] = 1.0;          // Wmintaunu_400M600
	filterEffMap[203656] = 1.0;          // Wmintaunu_600M800
	filterEffMap[203657] = 1.0;          // Wmintaunu_800M1000
	filterEffMap[203658] = 1.0;          // Wmintaunu_1000M1250
	filterEffMap[203659] = 1.0;          // Wmintaunu_1250M1500
	filterEffMap[203660] = 1.0;          // Wmintaunu_1500M1750
	filterEffMap[203661] = 1.0;          // Wmintaunu_1750M2000
	filterEffMap[203662] = 1.0;          // Wmintaunu_2000M2250
	filterEffMap[203663] = 1.0;          // Wmintaunu_2250M2500
	filterEffMap[203664] = 1.0;          // Wmintaunu_2500M2750
	filterEffMap[203665] = 1.0;          // Wmintaunu_2750M3000
	filterEffMap[203666] = 1.0;          // Wmintaunu_3000M3500
	filterEffMap[203667] = 1.0;          // Wmintaunu_3500M4000
	filterEffMap[203668] = 1.0;          // Wmintaunu_4000M4500
	filterEffMap[203669] = 1.0;          // Wmintaunu_4500M5000
	filterEffMap[203670] = 1.0;          // Wmintaunu_5000M



	// Top Samples //
	filterEffMap[110401] = 0.54316;      // ttbar_nonallhad
	filterEffMap[110070] = 1.0;          // singletop_tchan_lept_top
	filterEffMap[110071] = 1.0;          // singletop_tchan_lept_antitop
	filterEffMap[110302] = 1.0;          // st_schan_lep
	filterEffMap[110305] = 1.0;          // st_Wtchan_incl



	// Signal Samples //
	filterEffMap[182916] = 1.0;          // ZPrime ee 2 TeV
	filterEffMap[182917] = 1.0;          // ZPrime ee 3 TeV
	filterEffMap[182918] = 1.0;          // ZPrime ee 4 TeV
	filterEffMap[182919] = 1.0;          // ZPrime ee 5 TeV

	filterEffMap[182920] = 1.0;          // ZPrime mumu 2 TeV
	filterEffMap[182921] = 1.0;          // ZPrime mumu 3 TeV
	filterEffMap[182922] = 1.0;          // ZPrime mumu 4 TeV
	filterEffMap[182923] = 1.0;          // ZPrime mumu 5 TeV


	filterEffMap[203671] = 1.0;          // WPrime emutau 2 TeV
	filterEffMap[158762] = 1.0;          // WPrime emutau 3 TeV
	filterEffMap[203672] = 1.0;          // WPrime emutau 4 TeV
	filterEffMap[203673] = 1.0;          // WPrime emutau 5 TeV


	filterEffMap[158838] = 1.0;          // Wimp D52 DM1 MS1000
	filterEffMap[158839] = 1.0;          // Wimp D52 DM100 MS1000
	filterEffMap[158843] = 1.0;          // Wimp D52 DM1300 MS1000


	try{
		return filterEffMap[datasetID];
	}catch(const std::out_of_range& oor){
		std::cout<<"LPXKfactorTool: Failed to find filter efficiency for dataset ID " <<datasetID;
	}
	return 0.0;

}
