double Wplus_ct10nnlo_pdf_up(double x) {
   const int fNp = 40, fKstep = 0;
   const double fDelta = -1, fXmin = 9, fXmax = 9703;
   const double fX[40] = { 9, 14, 18, 23, 28,
                        33, 38, 43, 48, 53,
                        58, 63, 71, 81, 91,
                        106, 128, 154, 186, 225,
                        271, 328, 396, 477, 576,
                        696, 840, 1014, 1224, 1478,
                        1784, 2153, 2599, 3137, 3787,
                        4571, 5517, 6660, 8039, 9703 };
   const double fY[40] = { 18.15, 13.03, 10.09, 7.94, 6.63,
                        5.8, 5.23, 4.83, 4.54, 4.33,
                        4.17, 4.04, 3.85, 3.72, 3.6,
                        3.45, 3.26, 3.09, 2.96, 2.86,
                        2.79, 2.78, 2.82, 3, 3.3,
                        3.72, 4.27, 4.97, 5.85, 6.97,
                        8.41, 10.33, 13.03, 17.13, 23.71,
                        35.48, 58.59, 108.69, 228.25, 1486.31 };
   const double fB[40] = { -1.15709, -0.875654, -0.584579, -0.321077, -0.207114,
                        -0.134468, -0.0950138, -0.0674768, -0.0490791, -0.0362067,
                        -0.028094, -0.0254173, -0.0189299, -0.0111066, -0.0116438,
                        -0.0091212, -0.00783362, -0.00527257, -0.00316439, -0.00207792,
                        -0.000848234, 0.000190215, 0.00131447, 0.00278168, 0.00326945,
                        0.00368272, 0.00393104, 0.00410239, 0.00428596, 0.00453198,
                        0.00490721, 0.0055195, 0.00671578, 0.00847837, 0.0124338,
                        0.0165803, 0.0394559, 0.0257559, 0.257486, 1.41454 };
   const double fC[40] = { 0.0235647, 0.0327216, 0.040047, 0.0126535, 0.0101391,
                        0.00439, 0.00350087, 0.00200653, 0.00167299, 0.000901488,
                        0.000721054, -0.000185704, 0.00099663, -0.000214304, 0.000160584,
                        7.58765e-06, 5.0939e-05, 4.7563e-05, 1.83176e-05, 9.54045e-06,
                        1.71919e-05, 1.02652e-06, 1.55067e-05, 2.60704e-06, 2.3199e-06,
                        1.124e-06, 6.00501e-07, 3.84235e-07, 4.8992e-07, 4.78671e-07,
                        7.47547e-07, 9.1178e-07, 1.77048e-06, 1.5057e-06, 4.57956e-06,
                        7.09362e-07, 2.3472e-05, -3.5458e-05, 0.0002035, 1664 };
   const double fD[40] = { 0.000610455, 0.000610455, -0.00182624, -0.000167622, -0.000383276,
                        -5.92756e-05, -9.96221e-05, -2.2236e-05, -5.14338e-05, -1.20289e-05,
                        -6.04505e-05, 4.92639e-05, -4.03644e-05, 1.24963e-05, -3.39993e-06,
                        6.56838e-07, -4.32814e-08, -3.0464e-07, -7.50183e-08, 5.54452e-08,
                        -9.45343e-08, 7.09811e-08, -5.30849e-08, -9.66801e-10, -3.32192e-09,
                        -1.21181e-09, -4.14303e-10, 1.67754e-10, -1.47632e-11, 2.92894e-10,
                        1.48358e-10, 6.41776e-10, -1.6405e-10, 1.57634e-09, -1.64549e-09,
                        8.02065e-09, -1.71857e-08, 5.77612e-08, 5.77612e-08, 640.495 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}
