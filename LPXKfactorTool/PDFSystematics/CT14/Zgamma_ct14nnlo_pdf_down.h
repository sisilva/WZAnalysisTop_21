double Zgamma_ct14nnlo_pdf_down(double x) {
   const int fNp = 40, fKstep = 0;
   const double fDelta = -1, fXmin = 9, fXmax = 9703;
   const double fX[40] = { 9, 14, 18, 23, 28,
                        33, 38, 43, 48, 53,
                        58, 63, 71, 81, 91,
                        106, 128, 154, 186, 225,
                        271, 328, 396, 477, 576,
                        696, 840, 1014, 1224, 1478,
                        1784, 2153, 2599, 3137, 3787,
                        4571, 5517, 6660, 8039, 9703 };
   const double fY[40] = { -7.41, -6.56, -6.23, -5.95, -5.74,
                        -5.57, -5.41, -5.26, -5.11, -4.93,
                        -4.73, -4.52, -4.28, -4.35, -4.35,
                        -4.16, -3.82, -3.72, -3.69, -3.67,
                        -3.67, -3.7, -3.77, -3.95, -4.26,
                        -4.7, -5.25, -5.89, -6.62, -7.43,
                        -8.38, -9.69, -11.66, -14.63, -18.78,
                        -23.83, -31.88, -44.43, -62.2, -90.53 };
   const double fB[40] = { 0.245333, 0.109513, 0.0649883, 0.048037, 0.0368639,
                        0.0325075, 0.0311061, 0.0290679, 0.0326221, 0.0384436,
                        0.0416034, 0.0411428, 0.0110919, -0.009842, 0.00727613,
                        0.0163824, 0.0106057, 0.000691503, 0.000875358, 0.000239024,
                        -0.00025978, -0.000711769, -0.00154019, -0.00274782, -0.0034471,
                        -0.0038051, -0.00377937, -0.00358994, -0.00333949, -0.00308105,
                        -0.00323035, -0.00391475, -0.00492673, -0.0060841, -0.00634095,
                        -0.00710989, -0.00981646, -0.0118396, -0.0143085, -0.0202891 };
   const double fC[40] = { -0.0180356, -0.00912841, -0.0020027, -0.00138756, -0.000847053,
                        -2.42257e-05, -0.000256044, -0.000151597, 0.000862431, 0.000301872,
                        0.00033008, -0.000422192, -0.00333418, 0.00124079, 0.000471026,
                        0.000136056, -0.000398632, 1.73168e-05, -1.15713e-05, -4.74498e-06,
                        -6.09858e-06, -1.83106e-06, -1.03515e-05, -4.55756e-06, -2.50584e-06,
                        -4.77524e-07, 6.56256e-07, 4.32387e-07, 7.60237e-07, 2.5725e-07,
                        -7.45152e-07, -1.1096e-06, -1.15942e-06, -9.91831e-07, 5.9669e-07,
                        -1.57749e-06, -1.28358e-06, -4.86445e-07, -1.30389e-06, 1664 };
   const double fD[40] = { 0.00059381, 0.00059381, 4.10089e-05, 3.6034e-05, 5.48552e-05,
                        -1.54546e-05, 6.96318e-06, 6.76019e-05, -3.73706e-05, 1.88051e-06,
                        -5.01515e-05, -0.000121333, 0.000152499, -2.56587e-05, -7.44377e-06,
                        -8.10133e-06, 5.33267e-06, -3.00917e-07, 5.83443e-08, -9.80868e-09,
                        2.49563e-08, -4.1767e-08, 2.38435e-08, 6.90812e-09, 5.63422e-09,
                        2.62449e-09, -4.28868e-10, 5.20396e-10, -6.60087e-10, -1.09194e-09,
                        -3.29224e-10, -3.72281e-11, 1.03831e-10, 8.14626e-10, -9.24395e-10,
                        1.03562e-10, 2.32468e-10, -1.97593e-10, -1.97593e-10, 640.495 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}
