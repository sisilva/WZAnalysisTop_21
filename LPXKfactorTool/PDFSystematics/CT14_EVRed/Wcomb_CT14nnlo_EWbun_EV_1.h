double Wcomb_CT14nnlo_EWbun_EV_1(double x) {
   const int fNp = 49, fKstep = 0;
   const double fDelta = -1, fXmin = 6, fXmax = 10000;
   const double fX[49] = { 6, 10, 20, 30, 40,
                        50, 60, 70, 80, 90,
                        100, 125, 150, 175, 200,
                        250, 300, 400, 500, 600,
                        700, 800, 900, 1000, 1250,
                        1500, 1750, 2000, 2250, 2500,
                        2750, 3000, 3250, 3500, 3750,
                        4000, 4250, 4500, 5000, 5500,
                        6000, 6500, 7000, 7500, 8000,
                        8500, 9000, 9500, 10000 };
   const double fY[49] = { -0.238459, 2.0311, 2.89057, 2.91469, 3.19339,
                        3.25399, 3.20056, 3.33626, 3.36713, 3.30196,
                        3.32908, 3.23612, 3.40333, 3.31658, 3.33158,
                        3.39406, 3.33617, 3.37192, 3.40765, 3.46878,
                        3.52968, 3.50547, 3.58038, 3.57095, 3.56563,
                        3.4676, 3.36142, 3.25124, 3.09871, 2.98417,
                        2.8894, 2.77024, 2.64355, 2.42127, 2.3014,
                        1.99858, 1.79407, 1.45729, 0.665909, -0.45991,
                        -1.93379, -3.73976, -6.3547, -9.68056, -14.5553,
                        -21.9191, -33.6772, -54.3688, -93.9012 };
   const double fB[49] = { 0.795576, 0.365097, -0.0313605, 0.0254223, 0.0205158,
                        -0.00569562, 0.00441928, 0.0126988, -0.00524336, -0.00201544,
                        0.00189048, 0.000993015, 0.00304716, -0.00352659, 0.00244923,
                        -0.000294149, -0.000997552, 0.000699368, 0.000344635, 0.000827999,
                        4.28699e-06, 0.000255537, 0.000494591, -0.000269434, -0.000121579,
                        -0.000484476, -0.000391032, -0.000547748, -0.000570455, -0.000375278,
                        -0.000440109, -0.000431417, -0.000784422, -0.000618556, -0.000847209,
                        -0.00106483, -0.000981363, -0.00150521, -0.00183709, -0.00264963,
                        -0.00316255, -0.00437924, -0.00584596, -0.00788175, -0.0118306,
                        -0.0182269, -0.0299932, -0.0564986, -0.105357 };
   const double fC[49] = { -0.0635204, -0.0440993, 0.00445349, 0.00122479, -0.00171543,
                        -0.000905715, 0.0019172, -0.00108925, -0.000704963, 0.00102776,
                        -0.000637164, 0.000601265, -0.000519099, 0.000256149, -1.71165e-05,
                        -3.7751e-05, 2.36829e-05, -6.71371e-06, 3.16637e-06, 1.66728e-06,
                        -9.9044e-06, 1.24169e-05, -1.00264e-05, 2.38612e-06, -1.7947e-06,
                        3.43108e-07, 3.0667e-08, -6.57532e-07, 5.66706e-07, 2.14e-07,
                        -4.73324e-07, 5.08093e-07, -1.92011e-06, 2.58358e-06, -3.49819e-06,
                        2.6277e-06, -2.29382e-06, 1.98431e-07, -8.62195e-07, -7.62887e-07,
                        -2.6295e-07, -2.17044e-06, -7.62993e-07, -3.30859e-06, -4.58909e-06,
                        -8.20355e-06, -1.5329e-05, -3.76818e-05, 500 };
   const double fD[49] = { 0.00161843, 0.00161843, -0.000107624, -9.80073e-05, 2.69906e-05,
                        9.40973e-05, -0.000100215, 1.28097e-05, 5.77573e-05, -5.54973e-05,
                        1.65124e-05, -1.49382e-05, 1.03366e-05, -3.64354e-06, -1.37563e-07,
                        4.09559e-07, -1.01322e-07, 3.29336e-08, -4.99699e-09, -3.85723e-08,
                        7.44043e-08, -7.48109e-08, 4.1375e-08, -5.57442e-09, 2.85041e-09,
                        -4.16589e-10, -9.17599e-10, 1.63232e-09, -4.70275e-10, -9.16432e-10,
                        1.30856e-09, -3.23761e-09, 6.00493e-09, -8.10904e-09, 8.16786e-09,
                        -6.56202e-09, 3.323e-09, -7.07084e-10, 6.62052e-11, 3.33292e-10,
                        -1.27166e-09, 9.38295e-10, -1.69707e-09, -8.53664e-10, -2.40964e-09,
                        -4.75027e-09, -1.49019e-08, -1.49019e-08, 232.051 };
   int klow=0;
   if(x<=fXmin) klow=0;
   else if(x>=fXmax) klow=fNp-1;
   else {
     if(fKstep) {
       // Equidistant knots, use histogramming
       klow = int((x-fXmin)/fDelta);
       if (klow < fNp-1) klow = fNp-1;
     } else {
       int khig=fNp-1, khalf;
       // Non equidistant knots, binary search
       while(khig-klow>1)
         if(x>fX[khalf=(klow+khig)/2]) klow=khalf;
         else khig=khalf;
     }
   }
   // Evaluate now
   double dx=x-fX[klow];
   return (fY[klow]+dx*(fB[klow]+dx*(fC[klow]+dx*fD[klow])));
}
