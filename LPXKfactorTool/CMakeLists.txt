    # $Id: CMakeLists.txt 797759 2017-02-18 01:24:37Z rcreager $
    ################################################################################
    # Package: LPXKfactorTool
    ################################################################################
    
     
    # Declare the package name:
    atlas_subdir( LPXKfactorTool )
    
    # Extra dependencies based on the build environment:
    set( extra_deps )
    set( extra_libs )
    
    # Declare the package's dependencies:
    atlas_depends_on_subdirs(
     PUBLIC
     Control/xAODRootAccess 
     Control/AthToolSupport/AsgTools 
     PhysicsAnalysis/AnalysisCommon/PATInterfaces 
     Event/xAOD/xAODBase
     Event/xAOD/xAODTruth
       ${extra_deps} )
    
    # External dependencies:
    find_package( Boost )
    find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO REQUIRED )
    find_package( GTest )
    
    # Libraries in the package:
    atlas_add_library( LPXKfactorTool#Lib
       LPXKfactorTool/*.h Root/*.cxx
       PUBLIC_HEADERS LPXKfactorTool
       PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${BOOST_INCLUDE_DIRS}
       LINK_LIBRARIES xAODRootAccess AsgTools PATInterfaces xAODEventFormat xAODEventInfo xAODTruth ${extra_libs} ${ROOT_LIBRARIES} )

#  atlas_add_root_dictionary( LPXKfactorToolLib LPXKfactorToolDictSrc
#     ROOT_HEADERS LPXKfactorTool/*.h Root/LinkDef.h
#     EXTERNAL_PACKAGES ROOT )
