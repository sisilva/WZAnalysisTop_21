#include "MyWlnuSelection/doPFOHRElectron.h"

namespace top {
    // Select only events with positive electron
    bool doPFOHRElectron::apply(const top::Event& event) const {
        event.m_info->auxdecor<bool>("doPFOHRElectron") = true;
        return true;

    }
    //for (auto & mu : event.m_muons) 

    //For the cutflow and terminal output
    std::string doPFOHRElectron::name() const {
        return "DO_PFOHR_EL";
    }
}
