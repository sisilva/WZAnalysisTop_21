#include "MyWlnuSelection/CustomEventSaver.h"
#include "TopEvent/EventTools.h"
#include "TopEvent/Event.h"
#include "TopEventSelectionTools/TreeManager.h"

#include <TRandom3.h>

#include <exception>

namespace top {
    ///-- Constrcutor --///
    CustomEventSaver::CustomEventSaver() :
        //m_randomNumber(0.),
        //m_someOtherVariable(0.)
        m_isElTight(false),
        m_isElIsoHighPt(false),
        m_isElIsoTightTrackOnly(false),
        m_elIdTightSF(1.0),
        m_elIsoHighPtSF(1.0),
        m_elIsoTightTrackOnlySF(1.0),
        m_sumET_PFOHR(0.),
        m_hadrecoil(nullptr),
        m_kfactorTool(nullptr),
        m_weight_kfactor(1.0)
    {
    }

    ///-- initialize - done once at the start of a job before the loop over events --///
    void CustomEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
    {
        ///-- Let the base class do all the hard work --///
        ///-- It will setup TTrees for each systematic with a standard set of variables --///
        top::EventSaverFlatNtuple::initialize(config, file, extraBranches);

        // Initialize AsgElectronLikelihoodTool
        m_ElTightLH = new AsgElectronLikelihoodTool("TightLH");
        top::check(m_ElTightLH->setProperty("WorkingPoint", "TightLHElectron"),
                "Failed to set config for AsgElectronLikelihoodTool");
        top::check(m_ElTightLH->initialize(),
                "Failed to initialize AsgElectronLikelihoodTool");

        // Initialize IsolationSelectionTool
        m_isoElHighPt = new CP::IsolationSelectionTool("iso1");
        m_isoElTightTrackOnly = new CP::IsolationSelectionTool("iso2");
        top::check(m_isoElHighPt->setProperty("ElectronWP",
            "FixedCutHighPtCaloOnly"),
            "Failed to set config for IsolationSelectionTool");
        top::check(m_isoElHighPt->initialize(),
            "Failed to initialize IsolationSelectionTool");
        top::check(m_isoElTightTrackOnly->setProperty("ElectronWP",
            "FixedCutTightTrackOnly"),
            "Failed to set config for IsolationSelectionTool");
        top::check(m_isoElTightTrackOnly->initialize(),
            "Failed to initialize IsolationSelectionTool");

        // Initialize ElectronEfficiencyCorrectionTool
        std::string mapFile = "ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v2/map6.txt";
        //std::string mapFile = "map6.txt";
        m_elIsoHighPtSFtool = new 
            AsgElectronEfficiencyCorrectionTool
            ("AsgElectronEfficiencyCorrectionTool");
        //top::check(m_elIsoHighPtSFtool->setProperty("MapFilePath", mapFile),
        //    "Failed to set correction tool map file");
        top::check(m_elIsoHighPtSFtool->setProperty("IdKey",
            "Tight"), "Failed to set id key");
        top::check(m_elIsoHighPtSFtool->setProperty("IsoKey",
            "FixedCutHighPtCaloOnly"), "Failed to set isolation key");
        top::check(m_elIsoHighPtSFtool->initialize(),
            "Failed to initialize isoHighPt ElectronEfficiencyCorrectionTool");

        m_elIsoTightTrackOnlySFtool = new 
            AsgElectronEfficiencyCorrectionTool
            ("Iso1_ElectronEfficiencyCorrectionTool");
        //top::check(m_elIsoTightTrackOnlySFtool->setProperty("MapFilePath", mapFile),
        //    "Failed to set correction tool map file");
        top::check(m_elIsoTightTrackOnlySFtool->setProperty("IdKey",
            "Tight"), "Failed to set id key");
        top::check(m_elIsoTightTrackOnlySFtool->setProperty("IsoKey",
            "FixedCutTightTrackOnly"), "Failed to set id key");
        top::check(m_elIsoTightTrackOnlySFtool->initialize(),
            "Failed to initialize isoTighTrack ElectronEfficiencyCorrectionTool");

        m_elIdTightSFtool = new 
            AsgElectronEfficiencyCorrectionTool
            ("ID_ElectronEfficiencyCorrectionTool");
        //top::check(m_elIdTightSFtool->setProperty("MapFilePath", mapFile),
        //    "Failed to set correction tool map file");
        top::check(m_elIdTightSFtool->setProperty("IdKey",
            "Tight"), "Failed to set id key");
        top::check(m_elIdTightSFtool->initialize(),
            "Failed to initialize idTight ElectronEfficiencyCorrectionTool");

        // HadronicRecoilPFO2016 initialization
        m_hadrecoil = new hadrecoil("recoilTool");
        top::check(m_hadrecoil->initialize(), "Failed to initialize hadrecoil.");

        // K factor
        m_kfactorTool = new LPXKfactorTool("LPXKfactorTool");
        top::check(m_kfactorTool->setProperty("isMC15",true),
                "Failed to set property for k-factor calculation");
        top::check(m_kfactorTool->initialize(),
                "Failed initialize k factor calculation");

        ///-- Loop over the systematic TTrees and add the custom variables --///
        for (auto systematicTree : treeManagers()) {
            //systematicTree->makeOutputVariable(m_randomNumber, "randomNumber");
            //systematicTree->makeOutputVariable(m_someOtherVariable,"someOtherVariable");
            systematicTree->makeOutputVariable(m_isElTight, "el_isTight");
            systematicTree->makeOutputVariable(m_isElIsoHighPt, "el_isIsoHighPt");
            systematicTree->makeOutputVariable(m_isElIsoTightTrackOnly,
                "el_isIsoTightTrackOnly");
            systematicTree->makeOutputVariable(m_elIsoHighPtSF,
                "el_IsoHighPtSF");
            systematicTree->makeOutputVariable(m_elIsoTightTrackOnlySF,
                "el_IsoTightTrackOnlySF");
            systematicTree->makeOutputVariable(m_elIdTightSF, "el_IdTightSF");
            systematicTree->makeOutputVariable(m_PFOHR, "PFOHR");
            systematicTree->makeOutputVariable(m_sumET_PFOHR, "sumET_PFOHR");
            systematicTree->makeOutputVariable(m_weight_kfactor, "weight_KFactor");
        }
    }

    ///-- saveEvent - run for every systematic and every event --///
    void CustomEventSaver::saveEvent(const top::Event& event) 
    {
        ///-- set our variables to zero --///
        //m_randomNumber = 0.;
        //m_someOtherVariable = 0.;

        // Apply ID and isolation selections
        m_isElTight = false;
        m_isElIsoHighPt= false;
        m_isElIsoTightTrackOnly = false;
        m_elIsoHighPtSF = 1.0;
        m_elIsoTightTrackOnlySF = 1.0;
        m_elIdTightSF = 1.0;
        for (const auto elItr : event.m_electrons) {
            if (elItr->pt() >= 20000) {
                const xAOD::Electron m_electron = *elItr;
                if (m_ElTightLH->accept(elItr)) {
                    m_isElTight = true;
                    if (event.m_info->eventType(xAOD::EventInfo::IS_SIMULATION))
                        top::check(m_elIdTightSFtool->getEfficiencyScaleFactor(m_electron,
                                    m_elIdTightSF), "Failed to get TightLH ID SF.");
                }
                if (m_isoElTightTrackOnly->accept(m_electron)) {
                    m_isElIsoTightTrackOnly = true;
                    if (event.m_info->eventType(xAOD::EventInfo::IS_SIMULATION))
                        top::check(m_elIsoTightTrackOnlySFtool->getEfficiencyScaleFactor(m_electron,
                                    m_elIsoTightTrackOnlySF), "Failed to get FixedCutTightTrackOnly SF.");
                }
                if (elItr->pt() >= 60000) {
                    if (m_isoElHighPt->accept(m_electron)) {
                        m_isElIsoHighPt = true;
                        if (event.m_info->eventType(xAOD::EventInfo::IS_SIMULATION))
                            top::check(m_elIsoHighPtSFtool->getEfficiencyScaleFactor(m_electron,
                                        m_elIsoHighPtSF), "Failed to get FixedCutHighPtCaloOnly");
                    }
                }
            }
        }

        // Calculation of hadronic recoil variables
        try {
            if (event.m_info->auxdata<bool>("doPFOHRMuon")) {
                const xAOD::MuonContainer *FinalMuons;
                top::check(evtStore()->retrieve(FinalMuons, "Muons_TDS"),
                        "Failed to retrieve muons");
                xAOD::IParticleContainer *leptons = (xAOD::IParticleContainer *)FinalMuons;
                m_PFOHR = m_hadrecoil->getHadRecoilPFO(*(evtStore()->event()), leptons);
                m_sumET_PFOHR = m_hadrecoil->getSumET_PFO(*(evtStore()->event()), leptons);
            }
        }
        catch(std::exception &e) {;}
        try {
            if (event.m_info->auxdata<bool>("doPFOHRElectron")) {
                const xAOD::ElectronContainer *FinalElectrons;
                top::check (evtStore()->retrieve(FinalElectrons, "Electrons"),
                        "Failed to retrieve electrons");
                xAOD::IParticleContainer *leptons = (xAOD::IParticleContainer *)FinalElectrons;
                m_PFOHR = m_hadrecoil->getHadRecoilPFO(*(evtStore()->event()), leptons);
                m_sumET_PFOHR = m_hadrecoil->getSumET_PFO(*(evtStore()->event()), leptons);
            }
        }
        catch(std::exception &e) {;}

        if (event.m_info->eventType(xAOD::EventInfo::IS_SIMULATION)) {
            top::check(m_kfactorTool->getKFactor(m_weight_kfactor), "Error getting LPXKfactor.");
        }
        ///-- Fill them - you should probably do something more complicated --///
        //TRandom3 random( event.m_info->eventNumber() );
        //m_randomNumber = random.Rndm();    
        //m_someOtherVariable = 42;

        ///-- Let the base class do all the hard work --///
        top::EventSaverFlatNtuple::saveEvent(event);
    }

}
