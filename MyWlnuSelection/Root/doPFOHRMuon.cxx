#include "MyWlnuSelection/doPFOHRMuon.h"

namespace top {
    // Select only events with positive electron
    bool doPFOHRMuon::apply(const top::Event& event) const {
        event.m_info->auxdecor<bool>("doPFOHRMuon") = true;
        return true;

    }
    //for (auto & mu : event.m_muons) 

    //For the cutflow and terminal output
    std::string doPFOHRMuon::name() const {
        return "DO_PFOHR_MU";
    }
}
