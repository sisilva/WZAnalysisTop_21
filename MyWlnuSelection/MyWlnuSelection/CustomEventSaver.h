#ifndef MYWLNUSELECTION_CUSTOMEVENTSAVER_H
#define MYWLNUSELECTION_CUSTOMEVENTSAVER_H

#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "HadronicRecoilPFO2016/hadrecoil.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "LPXKfactorTool/LPXKfactorTool.h"

/**
 * This class shows you how to extend the flat ntuple to include your own variables
 * 
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work 
 * 
 */

namespace top{
    class CustomEventSaver : public top::EventSaverFlatNtuple {
        public:
            ///-- Default constrcutor with no arguments - needed for ROOT --///
            CustomEventSaver();
            ///-- Destructor does nothing --///
            virtual ~CustomEventSaver(){}

            ///-- initialize function for top::EventSaverFlatNtuple --///
            ///-- We will be setting up out custom variables here --///
            virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches) override;

            ///-- Keep the asg::AsgTool happy --///
            virtual StatusCode initialize() override {return StatusCode::SUCCESS;}      

            ///-- saveEvent function for top::EventSaverFlatNtuple --///
            ///-- We will be setting our custom variables on a per-event basis --///
            virtual void saveEvent(const top::Event& event) override;

        private:
            ///-- Some additional custom variables for the output --///
            //float m_randomNumber;
            //float m_someOtherVariable;
            ///-- Hadronic Recoil variables --///
            TLorentzVector m_PFOHR;
            double m_sumET_PFOHR;
            hadrecoil *m_hadrecoil;

            // Tool for tag event if electron is tight
            AsgElectronLikelihoodTool *m_ElTightLH;
            bool m_isElTight;

            // Tool for tag event if electron is isolated HighPtCaloOnly
            CP::IsolationSelectionTool *m_isoElHighPt;
            CP::IsolationSelectionTool *m_isoElTightTrackOnly;
            bool m_isElIsoHighPt;
            bool m_isElIsoTightTrackOnly;

            // ElectronEfficiencyCorrectionTool for SFs
            AsgElectronEfficiencyCorrectionTool *m_elIdTightSFtool;
            AsgElectronEfficiencyCorrectionTool *m_elIsoTightTrackOnlySFtool;
            AsgElectronEfficiencyCorrectionTool *m_elIsoHighPtSFtool;
            double m_elIdTightSF;
            double m_elIsoHighPtSF;
            double m_elIsoTightTrackOnlySF;

            LPXKfactorTool *m_kfactorTool;
            double m_weight_kfactor;

            ///-- Tell RootCore to build a dictionary (we need this) --///
            ClassDef(top::CustomEventSaver, 0);
    };
}

#endif
