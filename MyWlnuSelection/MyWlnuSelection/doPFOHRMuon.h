#ifndef DO_HRPFO_MU_H
#define DO_HRPFO_MU_H
    
#include "TopEventSelectionTools/EventSelectorBase.h"
#include "TopEvent/Event.h"
    
namespace top {

    /**
     * This is a tool that enables HadronicRecoil using muons
     */
    class doPFOHRMuon : public top::EventSelectorBase {
        public:
            //This function sees every event.  If you return true then the event passes this "cut"
            bool apply(const top::Event& event) const override;

            //For humans, something short and catchy
            std::string name() const override;
    }; 

}
    
#endif

