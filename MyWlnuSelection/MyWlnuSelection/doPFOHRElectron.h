#ifndef DO_HRPFO_EL_H
#define DO_HRPFO_EL_H
    
#include "TopEventSelectionTools/EventSelectorBase.h"
#include "TopEvent/Event.h"
    
namespace top {

    /**
     * This is a tool that enables HadronicRecoil using electrons
     */
    class doPFOHRElectron : public top::EventSelectorBase {
        public:
            //This function sees every event.  If you return true then the event passes this "cut"
            bool apply(const top::Event& event) const override;

            //For humans, something short and catchy
            std::string name() const override;
    }; 

}
    
#endif

