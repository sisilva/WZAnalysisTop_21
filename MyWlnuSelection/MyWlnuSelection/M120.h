#ifndef M120_H
#define M120_H
	
#include "TopEventSelectionTools/EventSelectorBase.h"
#include "TopEvent/Event.h"
	
namespace top{

	/**
	 * W born mass cut in 120 GeV.
	 */
	class M120 : public top::EventSelectorBase {
		public:
			//This function sees every event.  If you return true then the event passes this "cut"
			bool apply(const top::Event& event) const override;

			//For humans, something short and catchy
			std::string name() const override;
	}; 

}
	
#endif

