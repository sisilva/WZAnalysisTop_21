// -*- C++ -*-
/* 
 * Author: 
 Implementation file for class hadrecoil
 Fabrice Balli <fabrice.balli@cern.ch>
 */


#include "HadronicRecoilPFO2016/hadrecoil.h"

struct DescendingEta:std::function<bool(const xAOD::IParticle*, const xAOD::IParticle*)> {
    bool operator()(const xAOD::IParticle* l, const xAOD::IParticle* r)  const {
        return l->eta() > r->eta();
    }
};
struct descendingEta:std::function<bool(fastjet::PseudoJet, fastjet::PseudoJet)> {
    bool operator()(fastjet::PseudoJet l, fastjet::PseudoJet r)  const {
        return l.eta() > r.eta();
    }
};

// Constructor
hadrecoil::hadrecoil(const std::string& name, float Drcone, float MinDistCone, float neutralPtCut) :
    asg::AsgTool(name),
    drCone(Drcone),
    minDistCone(MinDistCone),
    m_neutralPtCut(neutralPtCut),
    m_weightPFOTool("WeightPFOTool"){
    }
// Sons of destruction
hadrecoil::~hadrecoil()  {}

double hadrecoil::getSumET_CaloCluster(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEM){
    const xAOD::VertexContainer* vertices = NULL;
    if ( !m_event.retrieve(vertices,"PrimaryVertices").isSuccess() )
    { // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve PrimaryVertices container. Exiting." );
        exit(10);
    }
    xAOD::VertexContainer::const_iterator vitr;
    for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
        if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx)
            pv = (*vitr);
    }
    hadrecoil::getCaloClusterContainer(m_event, doEM);
    double v = hadrecoil::sumETCorr_PFO( leptons, topoclusters, chargedSigPFOs);
    if (!hadrecoil::clean().isSuccess()) exit(10);
    return v;

}
double hadrecoil::getSumET_PFO(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEMCentral, bool doEMForward ){
    const xAOD::VertexContainer* vertices = NULL;
    if ( !m_event.retrieve(vertices,"PrimaryVertices").isSuccess() )
    { // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve PrimaryVertices container. Exiting." );
        exit(10);
    }
    xAOD::VertexContainer::const_iterator vitr;
    for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
        if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx)
            pv = (*vitr);
    }
    hadrecoil::getChargedGoodPFOContainer(m_event);
    hadrecoil::getNeutralPFOContainer(m_event, doEMCentral, doEMForward);
    double v = hadrecoil::sumETCorr_PFO( leptons, neutralPFOs, chargedSigPFOs);
    if (!hadrecoil::clean().isSuccess()) exit(10);
    return v;

}
double hadrecoil::getSumET_PFOCharged(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons){
    const xAOD::VertexContainer* vertices = NULL;
    if ( !m_event.retrieve(vertices,"PrimaryVertices").isSuccess() )
    { // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve PrimaryVertices container. Exiting." );
        exit(10);
    }
    xAOD::VertexContainer::const_iterator vitr;
    for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
        if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx)
            pv = (*vitr);
    }
    hadrecoil::getChargedGoodPFOContainer(m_event);
    double v = hadrecoil::sumETCorr_PFO( leptons, neutralPFOs, chargedSigPFOs);
    if (!hadrecoil::clean().isSuccess()) exit(10);
    return v;
}
double hadrecoil::getSumET_PFONeutral(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEMCentral, bool doEMForward){
    const xAOD::VertexContainer* vertices = NULL;
    if ( !m_event.retrieve(vertices,"PrimaryVertices").isSuccess() )
    { // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve PrimaryVertices container. Exiting." );
        exit(10);
    }
    xAOD::VertexContainer::const_iterator vitr;
    for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
        if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx)
            pv = (*vitr);
    }
    hadrecoil::getNeutralPFOContainer(m_event, doEMCentral, doEMForward);
    double v = hadrecoil::sumETCorr_PFO( leptons, neutralPFOs, chargedSigPFOs);
    if (!hadrecoil::clean().isSuccess()) exit(10);
    return v;
}
TLorentzVector hadrecoil::getHadRecoilPFOCharged(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons){
    const xAOD::VertexContainer* vertices = NULL;
    if ( !m_event.retrieve(vertices,"PrimaryVertices").isSuccess() )
    { // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve PrimaryVertices container. Exiting." );
        exit(10);
    }
    xAOD::VertexContainer::const_iterator vitr;
    for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
        if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx)
            pv = (*vitr); 
    }
    hadrecoil::getChargedGoodPFOContainer(m_event);

    //just for the random seed
    const xAOD::EventInfo* eventInfo = 0;
    if( !m_event.retrieve( eventInfo, "EventInfo").isSuccess() ) 
        std::cout << " Failed to retrieve event info." << std::endl;
    hole -> SetSeed(eventInfo->eventNumber());

    TLorentzVector v = hadrecoil::hadrecoilCorr_PFO(leptons, neutralPFOs, chargedSigPFOs);
    //TLorentzVector v = hadrecoil::hadrecoil_PFO(leptons, neutralPFOs, chargedSigPFOs);
    if (!hadrecoil::clean().isSuccess()) exit(10);
    return v;
}
TLorentzVector hadrecoil::getHadRecoilPFONeutral(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEMCentral, bool doEMForward){
    const xAOD::VertexContainer* vertices = NULL;
    if ( !m_event.retrieve(vertices,"PrimaryVertices").isSuccess() )
    { // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve PrimaryVertices container. Exiting." );
        exit(10);
    }
    xAOD::VertexContainer::const_iterator vitr;
    for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
        if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx)
            pv = (*vitr); 
    }

    hadrecoil::getNeutralPFOContainer(m_event, doEMCentral, doEMForward);
    //just for the random seed
    const xAOD::EventInfo* eventInfo = 0;
    if( !m_event.retrieve( eventInfo, "EventInfo").isSuccess() ) 
        std::cout << " Failed to retrieve event info." << std::endl;
    hole -> SetSeed(eventInfo->eventNumber());

    TLorentzVector v = hadrecoil::hadrecoilCorr_PFO(leptons, neutralPFOs, chargedPFOs);
    //TLorentzVector v = hadrecoil::hadrecoil_PFO(leptons, neutralPFOs, chargedSigPFOs);
    if (!hadrecoil::clean().isSuccess()) exit(10);
    return v;
}
TLorentzVector hadrecoil::getHadRecoilPFO(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEMCentral, bool doEMForward){
    const xAOD::VertexContainer* vertices = NULL;
    if ( !m_event.retrieve(vertices,"PrimaryVertices").isSuccess() )
    { // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve PrimaryVertices container. Exiting." );
        exit(10);
    }
    xAOD::VertexContainer::const_iterator vitr;
    for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
        if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx)
            pv = (*vitr); 
    }
    hadrecoil::getChargedGoodPFOContainer(m_event);
    hadrecoil::getNeutralPFOContainer(m_event, doEMCentral, doEMForward);

    //just for the random seed
    const xAOD::EventInfo* eventInfo = 0;
    if( !m_event.retrieve( eventInfo, "EventInfo").isSuccess() ) 
        std::cout << " Failed to retrieve event info." << std::endl;
    hole -> SetSeed(eventInfo->eventNumber());

    TLorentzVector v = hadrecoil::hadrecoilCorr_PFO(leptons, neutralPFOs, chargedSigPFOs);
    //TLorentzVector v = hadrecoil::hadrecoil_PFO(leptons, neutralPFOs, chargedSigPFOs);
    if (!hadrecoil::clean().isSuccess()) exit(10);
    return v;
}
TLorentzVector hadrecoil::getHadRecoilCaloCluster(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEM){
    const xAOD::VertexContainer* vertices = NULL;
    if ( !m_event.retrieve(vertices,"PrimaryVertices").isSuccess() )
    { // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve PrimaryVertices container. Exiting." );
        exit(10);
    }
    xAOD::VertexContainer::const_iterator vitr;
    for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
        if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx)
            pv = (*vitr);
    }
    hadrecoil::getCaloClusterContainer(m_event, doEM);
    //just for the random seed
    const xAOD::EventInfo* eventInfo = 0;
    if( !m_event.retrieve( eventInfo, "EventInfo").isSuccess() )
        std::cout << " Failed to retrieve event info." << std::endl;
    hole -> SetSeed(eventInfo->eventNumber());
    TLorentzVector v = hadrecoil::hadrecoilCorr_PFO(leptons, topoclusters, chargedSigPFOs);
    if (!hadrecoil::clean().isSuccess()) exit(10);
    return v;
}
TLorentzVector hadrecoil::getHadRecoilPFOVoronoi(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEMCentral, bool doEMForward){
    const xAOD::VertexContainer* vertices = NULL;
    if ( !m_event.retrieve(vertices,"PrimaryVertices").isSuccess() )
    { // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve PrimaryVertices container. Exiting." );
        exit(10);
    }
    xAOD::VertexContainer::const_iterator vitr;
    for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
        if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx)
            pv = (*vitr); 
    }
    hadrecoil::getChargedGoodPFOContainer(m_event);
    hadrecoil::getChargedPFOContainer(m_event);
    hadrecoil::getNeutralPFOContainer(m_event, doEMCentral, doEMForward);
    xAOD::PFOContainer* newNeutralPFO = new xAOD::PFOContainer();
    xAOD::PFOAuxContainer* newNeutralPFOAux = new xAOD::PFOAuxContainer();
    newNeutralPFO->setStore( newNeutralPFOAux );
    hadrecoil::MakeVoronoiNeutralPFOs(neutralPFOs, chargedPFOs, *newNeutralPFO, false);
    //TLorentzVector v = hadrecoil::hadrecoilCorr_PFO(leptons, newNeutralPFO, chargedSigPFOs);
    TLorentzVector v = hadrecoil::hadrecoil_PFO(leptons, newNeutralPFO, chargedSigPFOs);
    if (!hadrecoil::clean().isSuccess()) exit(10);
    delete newNeutralPFO;
    delete newNeutralPFOAux;
    return v;
}
TLorentzVector hadrecoil::getHadRecoilPFOVoronoiSpreading(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEMCentral, bool doEMForward){
    const xAOD::VertexContainer* vertices = NULL;
    if ( !m_event.retrieve(vertices,"PrimaryVertices").isSuccess() )
    { // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve PrimaryVertices container. Exiting." );
        exit(10);
    }
    xAOD::VertexContainer::const_iterator vitr;
    for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
        if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx)
            pv = (*vitr); 
    }
    hadrecoil::getChargedGoodPFOContainer(m_event);
    hadrecoil::getChargedPFOContainer(m_event);
    hadrecoil::getNeutralPFOContainer(m_event, doEMCentral, doEMForward);
    xAOD::PFOContainer* newNeutralPFO = new xAOD::PFOContainer();
    xAOD::PFOAuxContainer* newNeutralPFOAux = new xAOD::PFOAuxContainer();
    newNeutralPFO->setStore( newNeutralPFOAux );
    hadrecoil::MakeVoronoiNeutralPFOs(neutralPFOs, chargedPFOs, *newNeutralPFO, true);
    //TLorentzVector v = hadrecoil::hadrecoilCorr_PFO(leptons, newNeutralPFO, chargedSigPFOs);
    TLorentzVector v = hadrecoil::hadrecoil_PFO(leptons, newNeutralPFO, chargedSigPFOs);
    if (!hadrecoil::clean().isSuccess()) exit(10);
    delete newNeutralPFO;
    delete newNeutralPFOAux;
    return v;
}

TLorentzVector hadrecoil::getHadRecoilPFOSKVoronoi(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEMCentral, bool doEMForward){
    const xAOD::VertexContainer* vertices = NULL;
    if ( !m_event.retrieve(vertices,"PrimaryVertices").isSuccess() )
    { // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve PrimaryVertices container. Exiting." );
        exit(10);
    }
    xAOD::VertexContainer::const_iterator vitr;
    for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
        if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx)
            pv = (*vitr); 
    }
    double gridSpacing = 0.4;
    double rapmax = 2.5;
    double jetR=0.4;
    hadrecoil::getChargedGoodPFOContainer(m_event);
    hadrecoil::getChargedPFOContainer(m_event);
    hadrecoil::getNeutralPFOContainer(m_event, doEMCentral, doEMForward);
    std::vector<fastjet::PseudoJet> inputConst;
    for(const auto& pfo_itr : *neutralPFOs){
        TLorentzVector cl;
        cl.SetPtEtaPhiM( (pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), 0.);
        fastjet::PseudoJet PJ(cl.Px(),cl.Py(),cl.Pz(),cl.E());
        inputConst.push_back(PJ);
    }
    for(const auto& pfo_itr : *chargedPFOs){
        TLorentzVector cl;
        cl.SetPtEtaPhiM( (pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), 0.);
        fastjet::PseudoJet PJ(cl.Px(),cl.Py(),cl.Pz(),cl.E());
        inputConst.push_back(PJ);
    }
    softkiller *sk = new softkiller();
    std::vector<fastjet::PseudoJet> pfo = sk->createPflowClusters(chargedPFOs, neutralPFOs);
    //actually this method already filters the charged
    std::vector<fastjet::PseudoJet> pfoVoronoi = sk->createVoronoiClusters(pfo, hadrecoil::getRho(inputConst), jetR);
    std::vector<fastjet::PseudoJet> pfoSoftKiller = sk->createNeutralSKClusters(pfoVoronoi, gridSpacing, rapmax);
    xAOD::PFOContainer* newNeutralPFO = new xAOD::PFOContainer();
    xAOD::PFOAuxContainer* newNeutralPFOAux = new xAOD::PFOAuxContainer();
    newNeutralPFO->setStore( newNeutralPFOAux );
    for(auto clust : pfoSoftKiller){
        xAOD::PFO *newpfo = new xAOD::PFO();
        newpfo->setP4(clust.pt(), clust.eta(),clust.phi(),clust.m());
        newNeutralPFO->push_back(newpfo);
    }

    TLorentzVector v = hadrecoil::hadrecoilCorr_PFO(leptons, newNeutralPFO, chargedSigPFOs);
    if (!hadrecoil::clean().isSuccess()) exit(10);
    delete newNeutralPFO;
    delete newNeutralPFOAux;
    delete sk;
    return v;
}

double hadrecoil::sumET_PFO(xAOD::IParticleContainer* NeutralPFOs, xAOD::IParticleContainer* ChargedPFOs, xAOD::IParticleContainer* leptons){

    double sumet = 0.;

    for(const auto& pfo_itr : *NeutralPFOs){
        TLorentzVector pfo;
        pfo.SetPtEtaPhiE((pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), (pfo_itr)->e());
        //remove negative energy clusters
        if(pfo.E() < 0)continue;
        for(const auto& p : *leptons) //loop over leptons
            if(hadrecoil::deltaR( pfo.Eta(), pfo.Phi(), p->eta(), p->phi() ) < drCone ) sumet -= pfo.Pt();
        sumet+=pfo.Pt();
    }
    for(const auto& pfo_itr : *ChargedPFOs){
        TLorentzVector pfo;
        pfo.SetPtEtaPhiE((pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), (pfo_itr)->e());
        //remove negative energy clusters
        if(pfo.E() < 0)continue;
        for(const auto& p : *leptons) //loop over leptons
            if(hadrecoil::deltaR( pfo.Eta(), pfo.Phi(), p->eta(), p->phi() ) < drCone ) sumet -= pfo.Pt();
        sumet+=pfo.Pt();
    }
    return sumet;
}

double hadrecoil::sumETCorr_PFO( xAOD::IParticleContainer *container, xAOD::IParticleContainer* NeutralPFOs, xAOD::IParticleContainer* ChargedPFOs){

    double sumet = hadrecoil::sumET_PFO( NeutralPFOs, ChargedPFOs, container);
    TLorentzVector HR = hadrecoil::hadrecoil_PFO(container, NeutralPFOs, ChargedPFOs);
    rnd = hadrecoil::getRnd(HR, container);

    unsigned int i=0;
    for(const auto& p : *container){
        std::pair <double, double> s;
        s.first = p->eta();
        s.second = rnd[i]; 
        //std::cout<<"RND number SET = " <<rnd[i] <<std::endl;
        i++;
        //std::cout<<"cone Center phi : " <<coneCenter[i]<<std::endl;
        for(const auto& pfo_itr : *NeutralPFOs){
            if((pfo_itr)->e() < 0)continue;
            TLorentzVector pfot;
            pfot.SetPtEtaPhiE((pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), (pfo_itr)->e());
            if( hadrecoil::deltaR( pfot.Eta(), pfot.Phi(), s.first,  s.second) < drCone ){
                float angle = deltaPhi(p->phi(), pfot.Phi());
                if(p->phi() <  pfot.Phi())angle = -1. * angle;
                pfot.RotateZ(angle);
                sumet+=pfot.Pt();
            }
        }
        for(const auto& pfo_itr : *ChargedPFOs){
            TLorentzVector pfot;
            pfot.SetPtEtaPhiE((pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), (pfo_itr)->e());
            if((pfo_itr)->e() < 0)continue;
            if( hadrecoil::deltaR( pfot.Eta(), pfot.Phi(), s.first,  s.second) < drCone ){
                float angle = deltaPhi(p->phi(), pfot.Phi());
                if(p->phi() <  pfot.Phi())angle = -1. * angle;
                pfot.RotateZ(angle);
                sumet+=pfot.Pt();
            }
        }
    }
    return sumet;
}


std::vector<double> hadrecoil::getRnd(TLorentzVector HR, xAOD::IParticleContainer *container){
    std::vector<double> v;
    unsigned int seed;
    for(const auto& p : *container)
        seed = floor(p->pt()*1.e3) ;
    hole -> SetSeed(seed);
    //std::cout<<"seed = "<<seed<<std::endl;
    for(const auto& p : *container){ //loop over leptons
        bool isNextToPart = true, isNextToHR = true;
        double Rnd;
        while(isNextToPart || isNextToHR ){
            isNextToPart = false; isNextToHR = true;
            Rnd = hole->Uniform( -TMath::Pi(), TMath::Pi() );
            if(hadrecoil::deltaR(HR.Eta(), HR.Phi(), p->eta() ,Rnd) > minDistCone) isNextToHR = false;
            for(const auto& r : *container)
                if(hadrecoil::deltaR(p->eta(), Rnd, r->eta(), r->phi()) < minDistCone )isNextToPart = true;
        }
        v.push_back(Rnd);
        //std::cout<<"RND equal = " <<Rnd << std::endl;
    }
    return v;
}



TLorentzVector hadrecoil::sumClusters_PFO(xAOD::IParticleContainer* NeutralPFOs, xAOD::IParticleContainer* ChargedPFOs){
    TLorentzVector sumpfo;

    for(const auto& pfo_itr : *NeutralPFOs){
        if((pfo_itr)->pt() < 0 || (pfo_itr)->e() < 0)continue;
        TLorentzVector pfo;
        pfo.SetPtEtaPhiE((pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), (pfo_itr)->e());
        //remove negative energy clusters
        sumpfo+=pfo;
    }
    for(const auto& pfo_itr : *ChargedPFOs){
        if((pfo_itr)->pt() < 0 || (pfo_itr)->e() < 0)continue;
        TLorentzVector pfo;
        pfo.SetPtEtaPhiE((pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), (pfo_itr)->e());
        //remove negative energy clusters
        sumpfo+=pfo;
    }
    return sumpfo;
}

TLorentzVector hadrecoil::hadrecoil_PFO(xAOD::IParticleContainer *container, xAOD::IParticleContainer* NeutralPFOs, xAOD::IParticleContainer* ChargedPFOs){
    TLorentzVector sumClus = hadrecoil::sumClusters_PFO(NeutralPFOs, ChargedPFOs);
    ncones=container -> size();
    nNeutral=NeutralPFOs->size();
    nCharged = ChargedPFOs->size();
    for(const auto& pfo_itr : *NeutralPFOs){
        if((pfo_itr)->pt() < 0 || (pfo_itr)->e() < 0)continue;
        TLorentzVector pfo;
        pfo.SetPtEtaPhiE((pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), (pfo_itr)->e());
        for(const auto& part : *container){
            if(hadrecoil::deltaR( pfo.Eta(), pfo.Phi(), part->eta(), part->phi() ) < drCone) {
                sumClus -= pfo;
            }
        }
    }
    for(const auto& pfo_itr : *ChargedPFOs){
        if((pfo_itr)->pt() < 0 || (pfo_itr)->e() < 0)continue;
        TLorentzVector pfo;
        pfo.SetPtEtaPhiE((pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), (pfo_itr)->e());
        for(const auto& part : *container){
            if(hadrecoil::deltaR( pfo.Eta(), pfo.Phi(), part->eta(), part->phi() ) < drCone){
                sumClus -= pfo;
            }
        }
    }

    return sumClus;
}

TLorentzVector hadrecoil::hadrecoilCorr_PFO( xAOD::IParticleContainer *container, xAOD::IParticleContainer* NeutralPFOs, xAOD::IParticleContainer* ChargedPFOs){

    TLorentzVector HR = hadrecoil::hadrecoil_PFO(container, NeutralPFOs, ChargedPFOs);
    rnd = hadrecoil::getRnd(HR, container);
    unsigned int i =0 ;
    for(const auto& p : *container){ //loop over leptons
        std::pair <double, double> s;
        s.first = p->eta();
        s.second = rnd[i];
        //std::cout<<"RND number HR = " <<rnd[i] <<std::endl;
        i++; 
        //std::cout<<"cone Center phi : " <<coneCenter[i]<<std::endl;
        for(const auto& pfo_itr : *NeutralPFOs){
            if((pfo_itr)->e() < 0)continue;
            TLorentzVector pfot;
            pfot.SetPtEtaPhiE((pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), (pfo_itr)->e());
            if( hadrecoil::deltaR( pfot.Eta(), pfot.Phi(), s.first,  s.second) < drCone ){
                float angle = deltaPhi(p->phi(), pfot.Phi());
                if(p->phi() <  pfot.Phi())angle = -1. * angle;
                pfot.RotateZ(angle);
                HR+=pfot;
            }
        }
        for(const auto& pfo_itr : *ChargedPFOs){
            TLorentzVector pfot;
            pfot.SetPtEtaPhiE((pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), (pfo_itr)->e());
            if((pfo_itr)->e() < 0)continue;
            if( hadrecoil::deltaR( pfot.Eta(), pfot.Phi(), s.first,  s.second) < drCone ){
                float angle = deltaPhi(p->phi(), pfot.Phi());
                if(p->phi() <  pfot.Phi())angle = -1. * angle;
                pfot.RotateZ(angle);
                HR+=pfot;
            }
        }
    }
    return HR;
}

void hadrecoil::getChargedGoodPFOContainer(xAOD::TEvent& m_event){
    const xAOD::PFOContainer* pfos_charged; 
    if ( !m_event.retrieve( pfos_charged, "JetETMissChargedParticleFlowObjects" ).isSuccess() ){ // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve Clusters container. Exiting." );
        exit(10);
    }
    // create a shallow copy of the cluster container
    std::pair< xAOD::PFOContainer*, xAOD::ShallowAuxContainer* > pfos_charged_shallowCopy = xAOD::shallowCopyContainer( *pfos_charged );
    // iterate over our shallow copy
    xAOD::PFOContainer::iterator pfo_itr = (pfos_charged_shallowCopy.first)->begin();
    xAOD::PFOContainer::iterator pfo_end = (pfos_charged_shallowCopy.first)->end();

    for( ; pfo_itr != pfo_end; ++pfo_itr ) {

        float weight = 1.0;
        bool matchedToPrimaryVertex = (*pfo_itr)->auxdata<char>("DFCommonPFlow_PVMatched");

        if(!matchedToPrimaryVertex){
            if (  (*pfo_itr) == 0 ) {
                std::cout << "Have NULL pointer to charged PFO"  << std::endl;
                continue;
            }
            const xAOD::TrackParticle* ptrk = (*pfo_itr)->track(0);
            if ( ptrk == 0 ) {
                //	std::cout << "Skipping charged PFO with null track pointer." << std::endl;
                continue;
            }

            //vtz.z() provides z of that vertex w.r.t the center of the beamspot (z = 0). Thus we correct the track z0 to be w.r.t z = 0
            float z0 = ptrk->z0() + ptrk->vz();
            if (pv) {
                z0 = z0 - pv->z();
                float theta = ptrk->theta();
                if ( fabs(z0*sin(theta)) < 2.0 ) {
                    matchedToPrimaryVertex = true;
                }
            }// if pv available

            if (!matchedToPrimaryVertex) continue;

            int isInDenseEnvironment = false;
            bool gotVariable = (*pfo_itr)->attribute(xAOD::PFODetails::PFOAttributes::eflowRec_isInDenseEnvironment,isInDenseEnvironment);
            if(gotVariable && isInDenseEnvironment){
                if( ! m_weightPFOTool.fillWeight( (**pfo_itr), weight ).isSuccess() ) 	std::cout << "Cound not correct PFO energy" << std::endl; 
            }

            // now fill

            if( (*pfo_itr)->e()<0 ) continue;

            xAOD::PFO *newpfo = new xAOD::PFO();
            newpfo->makePrivateStore(**pfo_itr);
            newpfo->setP4( weight*newpfo->pt() ,newpfo->eta(),newpfo->phi(),newpfo->m()); //corrects 4-vector direction
            chargedSigPFOs -> push_back(newpfo);

        }else {
            //remove negative energy PFOs
            if( (*pfo_itr)->e()<0 ) continue;

            xAOD::PFO *newpfo = new xAOD::PFO();
            newpfo->makePrivateStore(**pfo_itr);
            newpfo->setP4(newpfo->auxdata<float>("DFCommonPFlow_CaloCorrectedPt"),newpfo->eta(),newpfo->phi(),newpfo->m()); //corrects 4-vector direction
            chargedSigPFOs -> push_back(newpfo);
        }
    }
    delete pfos_charged_shallowCopy.first;
    delete pfos_charged_shallowCopy.second;
    return;
}
void hadrecoil::getChargedPFOContainer(xAOD::TEvent& m_event){
    const xAOD::PFOContainer* pfos_charged; 
    if ( !m_event.retrieve( pfos_charged, "JetETMissChargedParticleFlowObjects" ).isSuccess() ){ // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve Clusters container. Exiting." );
        exit(10);
    }
    // create a shallow copy of the cluster container
    std::pair< xAOD::PFOContainer*, xAOD::ShallowAuxContainer* > pfos_charged_shallowCopy = xAOD::shallowCopyContainer( *pfos_charged );
    // iterate over our shallow copy
    xAOD::PFOContainer::iterator pfo_itr = (pfos_charged_shallowCopy.first)->begin();
    xAOD::PFOContainer::iterator pfo_end = (pfos_charged_shallowCopy.first)->end();

    for( ; pfo_itr != pfo_end; ++pfo_itr ) {
        //remove negative energy PFOs
        //if( (*pfo_itr)->e()<0 ) continue;
        xAOD::PFO *newpfo = new xAOD::PFO();
        newpfo->makePrivateStore(**pfo_itr);
        newpfo->setP4(newpfo->auxdata<float>("DFCommonPFlow_CaloCorrectedPt"),newpfo->eta(),newpfo->phi(),newpfo->m());
        chargedPFOs -> push_back(newpfo);
    }
    delete pfos_charged_shallowCopy.first;
    delete pfos_charged_shallowCopy.second;
    return;
}

void hadrecoil::getNeutralPFOContainer(xAOD::TEvent& m_event, bool doEMCentral, bool doEMForward){

    const xAOD::PFOContainer* pfos_Neutral; 
    if ( !m_event.retrieve( pfos_Neutral, "JetETMissNeutralParticleFlowObjects" ).isSuccess() ){ // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve Clusters container. Exiting." );
        exit(10);
    }
    // create a shallow copy of the cluster container
    std::pair< xAOD::PFOContainer*, xAOD::ShallowAuxContainer* > pfos_neutral_shallowCopy = xAOD::shallowCopyContainer( *pfos_Neutral );
    // iterate over our shallow copy
    xAOD::PFOContainer::iterator pfo_itr = (pfos_neutral_shallowCopy.first)->begin();
    xAOD::PFOContainer::iterator pfo_end = (pfos_neutral_shallowCopy.first)->end();

    for( ; pfo_itr != pfo_end; ++pfo_itr ) {
        //remove negative energy PFOs
        //if( (*pfo_itr)->e()<0 ) continue;
        /*xAOD::PFO *newpfo = new xAOD::PFO();
          newpfo->makePrivateStore(**pfo_itr);

          if(doEM &&  newpfo->isAvailable<float>("ptEM") &&  newpfo->isAvailable<float>("mEM") )
          newpfo->setP4( newpfo->auxdata<float>("ptEM") ,newpfo->eta(),newpfo->phi(),newpfo->auxdata<float>("mEM") ); 
          newpfo->setP4(newpfo->GetVertexCorrectedFourVec(*pv)); 
          if(newpfo->pt() < m_neutralPtCut)
          continue;
        //if(doEM )std::cout << "pt after vtx corr =  "<< newpfo->auxdata<float>("pt")   <<std::endl ;
        */

        if(doEMCentral && fabs((*pfo_itr)->eta()) < 2.5 && (*pfo_itr)->isAvailable<float>("ptEM") &&  (*pfo_itr)->isAvailable<float>("mEM"))
            (*pfo_itr)->setP4( (*pfo_itr)->auxdata<float>("ptEM") ,(*pfo_itr)->eta(),(*pfo_itr)->phi(),(*pfo_itr)->auxdata<float>("mEM") );
        if(doEMForward && fabs((*pfo_itr)->eta()) > 2.5 && (*pfo_itr)->isAvailable<float>("ptEM") &&  (*pfo_itr)->isAvailable<float>("mEM"))
            (*pfo_itr)->setP4( (*pfo_itr)->auxdata<float>("ptEM") ,(*pfo_itr)->eta(),(*pfo_itr)->phi(),(*pfo_itr)->auxdata<float>("mEM") );
        (*pfo_itr)->setP4((*pfo_itr)->GetVertexCorrectedFourVec(*pv)); 
        if((*pfo_itr)->pt() < m_neutralPtCut)
            continue;

        xAOD::PFO *newpfo = new xAOD::PFO();
        newpfo->makePrivateStore(**pfo_itr);
        neutralPFOs -> push_back( newpfo );
    }
    delete pfos_neutral_shallowCopy.first;
    delete pfos_neutral_shallowCopy.second;
    return;			
}


void hadrecoil::getCaloClusterContainer(xAOD::TEvent& m_event, bool doEM){
    const xAOD::CaloClusterContainer* clusters;
    if ( !m_event.retrieve( clusters, "CaloCalTopoClusters" ).isSuccess() ){ // retrieve arguments: container type, container key
        Error("execute()", "Failed to retrieve Clusters container. Exiting." );
        exit(10);
    }
    // create a shallow copy of the cluster container
    std::pair< xAOD::CaloClusterContainer*, xAOD::ShallowAuxContainer* > clusters_shallowCopy = xAOD::shallowCopyContainer( *clusters );

    // iterate over our shallow copy
    xAOD::CaloClusterContainer::iterator clus_itr = (clusters_shallowCopy.first)->begin();
    xAOD::CaloClusterContainer::iterator clus_end = (clusters_shallowCopy.first)->end();
    for( ; clus_itr != clus_end; ++clus_itr ) {
        if(doEM)
            (*clus_itr)->setSignalState(xAOD::CaloCluster::UNCALIBRATED);
        else
            (*clus_itr)->setSignalState(xAOD::CaloCluster::CALIBRATED);
        //remove negative energy clusters
        if((*clus_itr)->e() < 0)
            continue;
        xAOD::CaloCluster *cl = new xAOD::CaloCluster();
        cl->makePrivateStore(**clus_itr);
        if(doEM)
            cl->setSignalState(xAOD::CaloCluster::UNCALIBRATED);
        else
            cl->setSignalState(xAOD::CaloCluster::CALIBRATED);
        topoclusters->push_back(cl);
    }
    delete clusters_shallowCopy.first;
    delete clusters_shallowCopy.second;
    return;

}



void hadrecoil::MakeVoronoiNeutralPFOs(xAOD::PFOContainer* NeutralPFOs, xAOD::PFOContainer* ChargedPFOs, xAOD::PFOContainer &newPFOContainer, bool doSpreading){

    std::vector<fastjet::PseudoJet> inputConst;
    for(const auto& pfo_itr : *NeutralPFOs){
        if((pfo_itr)->pt() <0 || (pfo_itr)->e() <0)continue;
        TLorentzVector cl;
        cl.SetPtEtaPhiM( (pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), 0.);
        fastjet::PseudoJet PJ(cl.Px(),cl.Py(),cl.Pz(),cl.E());
        inputConst.push_back(PJ);
        xAOD::PFO *newpfo = new xAOD::PFO();
        newpfo->makePrivateStore(*pfo_itr);
        newPFOContainer.push_back( newpfo );
    }
    //add the charged PFOs - no pile-up suppression !!
    for(const auto& pfo_itr : *ChargedPFOs){
        if((pfo_itr)->pt() <0 || (pfo_itr)->e() <0)continue;
        TLorentzVector cl;
        cl.SetPtEtaPhiM( (pfo_itr)->pt(), (pfo_itr)->eta(), (pfo_itr)->phi(), 0.);
        fastjet::PseudoJet PJ(cl.Px(),cl.Py(),cl.Pz(),cl.E());
        inputConst.push_back(PJ);
        xAOD::PFO *newpfo = new xAOD::PFO();
        newpfo->makePrivateStore(*pfo_itr);
        newPFOContainer.push_back( newpfo );
    }
    //just to relate the PFOs to the clusters from fastjet
    std::sort(newPFOContainer.begin(), newPFOContainer.end(), DescendingEta());

    fastjet::Selector jselector = fastjet::SelectorAbsRapRange(0.0,2.1);
    fastjet::JetAlgorithm algo = fastjet::kt_algorithm;
    float jetR = 0.4;
    fastjet::JetDefinition jetDef(algo, jetR,fastjet::E_scheme, fastjet::Best);
    fastjet::AreaDefinition area_def(fastjet::voronoi_area, fastjet::VoronoiAreaSpec(0.9));

    fastjet::ClusterSequenceArea clustSeq(inputConst, jetDef, area_def);
    fastjet::JetMedianBackgroundEstimator bge(jselector,jetDef,area_def);

    bge.set_particles(inputConst);
    std::vector<fastjet::PseudoJet> inclusiveJets = fastjet::sorted_by_pt(clustSeq.inclusive_jets(0));

    //trying to get to the number of final constituents : is it the same as the number of input constituents ?
    std::vector< fastjet::PseudoJet > tempConstits;
    for(unsigned int iJet = 0 ; iJet < inclusiveJets.size() ; iJet++){
        fastjet::PseudoJet jet = inclusiveJets[iJet];
        std::vector<fastjet::PseudoJet> constituents = jet.constituents();
        for(unsigned int iCons = 0; iCons < constituents.size(); iCons++){ 
            tempConstits.push_back(constituents[iCons]);
        }
    }

    std::sort(tempConstits.begin(), tempConstits.end(),descendingEta());
    hadrecoil::applyVoronoiSubtraction(newPFOContainer,*&tempConstits, bge.rho());
    //for(unsigned int iCons = 0; iCons < tempConstits.size(); iCons++)std::cout<<"fisrst constit pt = " << tempConstits[iCons].pt() <<std::endl;
    if(doSpreading)
        hadrecoil::applyVoronoiSpreading(newPFOContainer, *&tempConstits);

    //only keep neutral, remove charged
    xAOD::PFOContainer::iterator pfo_itr = newPFOContainer.begin();
    xAOD::PFOContainer::iterator pfo_end = newPFOContainer.end();
    for( ; pfo_itr != pfo_end; ) {
        //std::cout<<(*pfo_itr)->pt()<<std::endl;
        if(fabs((*pfo_itr)->charge() ) >0.5){
            newPFOContainer.erase(pfo_itr);
            pfo_end--;
            continue;
        }
        else ++pfo_itr;
    }
    return;
}

void hadrecoil::applyVoronoiSpreading(xAOD::PFOContainer &newPFOContainer, std::vector< fastjet::PseudoJet > &constituents){
    std::sort(constituents.begin(), constituents.end(),descendingEta());
    std::sort(newPFOContainer.begin(),newPFOContainer.end(),DescendingEta());
    if(constituents.size() !=  newPFOContainer.size() ){
        //AsgTools::ATH_MSG_WARNING( " Voronoi spreading : no same size for pflow and cluster constainers !! " );
        //AsgTools::ATH_MSG_WARNING( " Voronoi spreading : exiting " );
        std::cout << " Voronoi spreading : no same size for pflow and cluster constainers !! " << std::endl;
        std::cout << " Voronoi spreading : exiting " << std::endl;

        exit(10);
    }
    xAOD::PFOContainer *negativeConsts = new xAOD::PFOContainer();
    xAOD::PFOContainer *positiveConsts = new xAOD::PFOContainer();
    xAOD::PFOAuxContainer *negativeConstsAux = new xAOD::PFOAuxContainer();
    xAOD::PFOAuxContainer *positiveConstsAux = new xAOD::PFOAuxContainer();
    negativeConsts->setStore( negativeConstsAux );
    positiveConsts->setStore( positiveConstsAux );
    xAOD::PFOContainer::iterator pfo_itr = newPFOContainer.begin();
    xAOD::PFOContainer::iterator pfo_end = newPFOContainer.end();
    for( ; pfo_itr != pfo_end; ++pfo_itr) {
        //std::cout<<"hello world 0.2 " <<std::endl;
        if(fabs((*pfo_itr)->charge() ) > 0.5 )continue;
        xAOD::PFO *newpfo = new xAOD::PFO();
        newpfo->makePrivateStore(*(*pfo_itr));
        if((*pfo_itr)->pt()<0.)negativeConsts->push_back(newpfo);
        if((*pfo_itr)->pt()>0.)positiveConsts->push_back(newpfo);
        //std::cout<<"hello world 0.2 " <<(*pfo_itr)->pt()<<std::endl;
    }
    xAOD::PFOContainer::iterator cl_itr = negativeConsts->begin();
    xAOD::PFOContainer::iterator cl_end = negativeConsts->end();

    for( ; cl_itr != cl_end; ++cl_itr) {
        float sumw = 0.;
        xAOD::PFOContainer::iterator clp_itr = positiveConsts->begin();
        xAOD::PFOContainer::iterator clp_end = positiveConsts->end();
        for( ; clp_itr != clp_end; ++clp_itr){
            float dr = hadrecoil::deltaR((*clp_itr)->eta(), (*clp_itr)->phi(), (*cl_itr)->eta(), (*cl_itr)->phi() );
            if( dr < 0.4 )sumw += 1./(dr * dr);
        }
        clp_itr = positiveConsts->begin();
        for( ; clp_itr != clp_end; ++clp_itr){
            float dr = hadrecoil::deltaR((*clp_itr)->eta(), (*clp_itr)->phi(), (*cl_itr)->eta(), (*cl_itr)->phi() ) ;
            if( dr > 0.4 )continue;
            float w = (1./ (dr *dr) ) / sumw ;
            float comp = w * fabs( (*cl_itr)->pt() );
            if(comp < (*clp_itr)->pt() ){
                float newptpos = (*clp_itr)->pt() - comp ;
                float newptneg = (*cl_itr)->pt() + comp ;
                (*clp_itr)->setP4(newptpos, (*clp_itr)->eta(), (*cl_itr)->phi());
                (*cl_itr)->setP4(newptneg, (*cl_itr)->eta(), (*cl_itr)->phi());
            }
            else if(comp > (*clp_itr)->pt() ){
                float newptpos = 0 ;
                float newptneg = (*cl_itr)->pt() + (*clp_itr)->pt() ;
                (*clp_itr)->setP4(newptpos, (*clp_itr)->eta(), (*cl_itr)->phi());
                (*cl_itr)->setP4(newptneg, (*cl_itr)->eta(), (*cl_itr)->phi());
            }
        }
    }
    newPFOContainer.clear();
    xAOD::PFOContainer::iterator clp_itr = positiveConsts->begin();
    xAOD::PFOContainer::iterator clp_end = positiveConsts->end();
    for( ; clp_itr != clp_end; ++clp_itr){
        if((*clp_itr)->pt() >0.){
            xAOD::PFO *newpfo = new xAOD::PFO();
            newpfo->makePrivateStore(*(*clp_itr));
            newPFOContainer.push_back(newpfo);
        }
    }
    cl_itr = negativeConsts->begin();
    for( ; cl_itr != cl_end; ++cl_itr)
        if((*cl_itr)->pt() >0.)	std::cout<< (*cl_itr)->pt() <<std::endl;
    delete negativeConsts;
    delete positiveConsts;
    delete negativeConstsAux;
    delete positiveConstsAux;
    return;	
}

void hadrecoil::applyVoronoiSubtraction(xAOD::PFOContainer &newPFOContainer, std::vector< fastjet::PseudoJet > &constituents, float rho){
    std::sort(constituents.begin(), constituents.end(),descendingEta());
    std::sort(newPFOContainer.begin(),newPFOContainer.end(),DescendingEta());
    if(constituents.size() !=  newPFOContainer.size() ){
        std::cout << " Voronoi spreading : no same size for pflow and cluster constainers !! " << std::endl;
        std::cout << " Voronoi spreading : exiting " << std::endl;
        exit(10);
    }
    for(unsigned int iCons = 0; iCons < constituents.size(); iCons++){
        if(fabs(newPFOContainer.at(iCons)->charge() ) > 0.5 )continue;
        float area = constituents[iCons].area();
        float correctedPt = constituents[iCons].pt()-rho*area;
        //constituents[iCons].reset_momentum_PtYPhiM(correctedPt, constituents[iCons].rap(), constituents[iCons].phi());
        newPFOContainer.at(iCons)->setP4(correctedPt, constituents[iCons].eta(), constituents[iCons].phi());
        //std::cout << " coorected eta = "<<constituents[iCons].eta()<<std::endl;
        //std::cout << " eta = "<<(newPFOContainer.at(iCons))->eta()<<std::endl;
    }// end loop over constits
    return;
}


float hadrecoil::getRho(std::vector< fastjet::PseudoJet > inputConst){
    fastjet::Selector jselector = fastjet::SelectorAbsRapRange(0.0,2.1);
    fastjet::JetAlgorithm algo = fastjet::kt_algorithm;
    float jetR = 0.4;
    fastjet::JetDefinition jetDef(algo, jetR,fastjet::E_scheme, fastjet::Best);
    fastjet::AreaDefinition area_def(fastjet::voronoi_area, fastjet::VoronoiAreaSpec(0.9));
    fastjet::JetMedianBackgroundEstimator bge(jselector,jetDef,area_def);
    bge.set_particles(inputConst);
    return bge.rho();
}

StatusCode  hadrecoil::initialize(){

    chargedPFOs = new xAOD::PFOContainer();
    chargedSigPFOs = new xAOD::PFOContainer();
    neutralPFOs = new xAOD::PFOContainer();
    topoclusters = new xAOD::CaloClusterContainer();
    chargedPFOsAux = new xAOD::PFOAuxContainer();
    chargedSigPFOsAux = new xAOD::PFOAuxContainer();
    neutralPFOsAux = new xAOD::PFOAuxContainer();
    topoclustersAux = new xAOD::CaloClusterAuxContainer();

    chargedPFOs->setStore( chargedPFOsAux );
    chargedSigPFOs->setStore( chargedSigPFOsAux );
    neutralPFOs->setStore( neutralPFOsAux );
    topoclusters->setStore(topoclustersAux);

    hole = new TRandom3();
    ncones=0;
    nCharged = 0;
    nNeutral=0;

    return StatusCode::SUCCESS;
}
StatusCode  hadrecoil::finalize(){
    delete chargedPFOs;
    delete chargedSigPFOs;
    delete neutralPFOs;
    delete topoclusters;
    delete chargedPFOsAux;
    delete chargedSigPFOsAux;
    delete neutralPFOsAux;
    delete topoclustersAux;
    delete hole;
    return StatusCode::SUCCESS;
}
StatusCode  hadrecoil::clean(){
    chargedPFOs->clear();
    chargedSigPFOs->clear();
    neutralPFOs->clear();
    topoclusters->clear();
    rnd.clear();
    return StatusCode::SUCCESS;	
}



/*
   double hadrecoil::sumET(xAOD::TEvent& m_event){

   const xAOD::CaloClusterContainer* clusters; 
   if ( !m_event.retrieve( clusters, "CaloCalTopoClusters" ).isSuccess() ){ // retrieve arguments: container type, container key
   Error("execute()", "Failed to retrieve Clusters container. Exiting." );
   exit(10);
   }
// create a shallow copy of the cluster container
std::pair< xAOD::CaloClusterContainer*, xAOD::ShallowAuxContainer* > clusters_shallowCopy = xAOD::shallowCopyContainer( *clusters );

// iterate over our shallow copy
xAOD::CaloClusterContainer::iterator clus_itr = (clusters_shallowCopy.first)->begin();
xAOD::CaloClusterContainer::iterator clus_end = (clusters_shallowCopy.first)->end();
double sumet = 0.;
for( ; clus_itr != clus_end; ++clus_itr ) {
TLorentzVector clust;
clust.SetPtEtaPhiE((*clus_itr)->pt(), (*clus_itr)->eta(), (*clus_itr)->phi(), (*clus_itr)->e());
//remove negative energy clusters
if(clust.E() < 0)continue;
sumet+=clust.Pt();
}

delete clusters_shallowCopy.first;
delete clusters_shallowCopy.second;
return sumet;
}
*/






