#ifndef HADRONICRECOILPFO2016_LINKDEF_H
#define HADRONICRECOILPFO2016_LINKDEF_H

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
// Declare the class(es) to generate dictionaries for:
#pragma link C++ class HadronicRecoilPFO2016+;

#endif // HADRONICRECOILPFO2016_LINKDEF_H
