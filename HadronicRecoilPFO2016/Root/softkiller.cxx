//Softkiller for hadronic recoil package

//authors : 	jennifer.roloff@cern.ch
//		fabrice.balli@cern.ch

#include "HadronicRecoilPFO2016/softkiller.h"



    softkiller::softkiller(){}
    softkiller::~softkiller(){}
///////////////////////////////////////////////////////////////////////////
//Creates PseudoJet collection of PFO
//Each pseudojet has a tag identifiying what type of PFO (HSCh, PUCh, Ne)
//////////////////////////////////////////////////////////////////////////
std::vector<fastjet::PseudoJet> softkiller::createPflowClusters(const xAOD::PFOContainer *m_pflow_charged, const xAOD::PFOContainer *m_pflow_neutral){
 /* if(m_vertices->size() == 0){
  	 std::cout<<"no vertex in the event, exiting"<<std::endl;
	 exit(10);
	 }
  const xAOD::Vertex& vtx = *m_vertices->at(0);*/
	std::vector<fastjet::PseudoJet> pfo;

  for( auto *iconst : *m_pflow_charged ){
    fastjet::PseudoJet constit =  fastjet::PseudoJet(iconst->p4());
    /*float z0 = iconst->track(0)->z0() + iconst->track(0)->vz() - vtx.z();
    float theta = iconst->track(0)->theta();

    if ( fabs(z0*sin(theta)) < 2.0 )  constit.set_user_info(new MyPFOInfo("HSCh") );
    else  constit.set_user_info(new MyPFOInfo("PUCh") );*/
    if ( (iconst)->auxdata<char>("DFCommonPFlow_PVMatched"))  constit.set_user_info(new MyPFOInfo("HSCh") );
    else  constit.set_user_info(new MyPFOInfo("PUCh") );
    pfo.push_back(constit);
    
  }

  for( auto *iconst : *m_pflow_neutral){
    fastjet::PseudoJet constit =  fastjet::PseudoJet(iconst->p4());
    constit.set_user_info(new MyPFOInfo("Ne") );
    pfo.push_back(constit);
  }

	return pfo;
}



//////////////////////////////////////////////////////////////////////////////// 
//Creates a new cluster collection with Voronoi subtraction applied to neutral
//Note that all PFO are used in calculating the Voronoi area
////////////////////////////////////////////////////////////////////////////////
std::vector<fastjet::PseudoJet>  softkiller::createVoronoiClusters(std::vector<fastjet::PseudoJet> clusters, double rho, double jetR){
  fastjet::JetDefinition jetDef(fastjet::kt_algorithm, jetR,fastjet::E_scheme, fastjet::Best);
  fastjet::AreaDefinition area_def(fastjet::voronoi_area, fastjet::VoronoiAreaSpec(0.9));
  fastjet::ClusterSequenceArea clustSeq(clusters, jetDef, area_def);
	std::vector<fastjet::PseudoJet> voronoiPFO;

  std::vector<fastjet::PseudoJet> inclusiveJets = sorted_by_pt(clustSeq.inclusive_jets(0));
  for(unsigned int iJet = 0 ; iJet < inclusiveJets.size() ; iJet++){
    std::vector<fastjet::PseudoJet> constituents = inclusiveJets[iJet].constituents();

    for(auto clust : constituents){
      float correctedPt = clust.pt() - rho * clust.area();
      float ptRat = correctedPt / clust.pt();
      fastjet::PseudoJet newClust = fastjet::PseudoJet( ptRat * clust );
      if( ptRat < 0) continue; //Applying zero-suppresion on Voronoi
			std::string pfoType = clust.user_info<MyPFOInfo>().getType();
			if(strcmp("Ne", pfoType.c_str()) == 0){
		    newClust.set_user_info(new MyPFOInfo("Ne") );
		 		voronoiPFO.push_back(newClust);
			}
			else voronoiPFO.push_back(clust);
    }
  } 

	return voronoiPFO;
}



////////////////////////////////////////////////////////////////////////
//Creates a new PFO collection with HS charged PFO and neutral PFO
//Neutrals have both Voronoi and SK applied
///////////////////////////////////////////////////////////////////////
std::vector<fastjet::PseudoJet> softkiller::createSKClusters(std::vector<fastjet::PseudoJet> PFO, double gridSpacing, double rapmax){
  fastjet::Selector selector = fastjet::SelectorAbsRapRange(0, rapmax);
  fastjet::RectangularGrid SKgrid(-rapmax, rapmax, gridSpacing, gridSpacing, selector);
  fastjet::contrib::SoftKiller softkilleR(SKgrid);

	double ptThreshold = 0;
	std::vector<fastjet::PseudoJet> SKPFOVoronoi;
	softkilleR.apply(PFO, SKPFOVoronoi, ptThreshold);
	//std::vector<fastjet::PseudoJet> softkillerPFO = applyThreshold(NeutralVoronoi, ptThreshold);
	//This keeps only the neutral	
	std::vector<fastjet::PseudoJet> softkillerPFO = softkiller::applyThreshold(SKPFOVoronoi, ptThreshold);
	//add back the cPFOs
	for(auto clust : PFO){
		if( strcmp("Ne", clust.user_info<MyPFOInfo>().getType().c_str()) == 0) continue;
		if( strcmp("PUCh", clust.user_info<MyPFOInfo>().getType().c_str()) == 0) continue;
		if( strcmp("HSCh", clust.user_info<MyPFOInfo>().getType().c_str()) == 0) softkillerPFO.push_back(clust); 
		}
	return softkillerPFO;
}

////////////////////////////////////////////////////////////////////////
//Creates a new PFO collection with HS charged PFO and neutral PFO
//Neutrals have both Voronoi and SK applied
///////////////////////////////////////////////////////////////////////
std::vector<fastjet::PseudoJet> softkiller::createNeutralSKClusters(std::vector<fastjet::PseudoJet> PFO, double gridSpacing, double rapmax){
  fastjet::Selector selector = fastjet::SelectorAbsRapRange(0, rapmax);
  fastjet::RectangularGrid SKgrid(-rapmax, rapmax, gridSpacing, gridSpacing, selector);
  fastjet::contrib::SoftKiller softkilleR(SKgrid);

	double ptThreshold = 0;
	std::vector<fastjet::PseudoJet> SKPFOVoronoi;
	softkilleR.apply(PFO, SKPFOVoronoi, ptThreshold);
	//std::vector<fastjet::PseudoJet> softkillerPFO = applyThreshold(NeutralVoronoi, ptThreshold);
	//This keeps only the neutral	
	std::vector<fastjet::PseudoJet> softkillerPFO = softkiller::applyThreshold(SKPFOVoronoi, ptThreshold);
	return softkillerPFO;
}


///////////////////////////////////////////////////////////////////////////////////////////////
//Applies SK to only neutral particles and eliminates charged PU from the clusters collection
///////////////////////////////////////////////////////////////////////////////////////////////
std::vector<fastjet::PseudoJet> softkiller::applyThreshold(std::vector<fastjet::PseudoJet> clusters, double threshold){
	std::vector<fastjet::PseudoJet> subtractedPFO;

	for(auto clust : clusters){
	//only apply this to neutral !
		//if( strcmp("HSCh", clust.user_info<MyPFOInfo>().getType().c_str()) == 0) subtractedPFO.push_back(clust);
		if( strcmp("HSCh", clust.user_info<MyPFOInfo>().getType().c_str()) == 0) continue;
		if( strcmp("PUCh", clust.user_info<MyPFOInfo>().getType().c_str()) == 0) continue;
		if( strcmp("Ne", clust.user_info<MyPFOInfo>().getType().c_str()) == 0){
			if(clust.pt() > threshold) subtractedPFO.push_back(clust);
		}
	}
	return subtractedPFO;
}





