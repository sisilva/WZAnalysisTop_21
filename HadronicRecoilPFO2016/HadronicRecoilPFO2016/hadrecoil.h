// -*- C++ -*-
/* 
 * Author: 
 	package to make hadronic recoil great again
 	Fabrice Balli <fabrice.balli@cern.ch>
 */


#ifndef HADRECOIL_H
#define HADRECOIL_H 1

//ROOT includes
#include "TRandom3.h"
#include "TLorentzVector.h"

// STL includes
#include <string>
#include <vector>

// FrameWork includes
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AsgTool.h"

//Fastjet includes
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/tools/JetMedianBackgroundEstimator.hh"
#include "fastjet/contrib/ConstituentSubtractor.hh"
#include "fastjet/tools/Subtractor.hh"


// EDM includes
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODPFlow/PFOContainer.h"
#include "xAODPFlow/PFOAuxContainer.h"
#include "PFlowUtils/WeightPFOTool.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODBase/IParticle.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODBase/IParticleHelpers.h"

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODCore/ShallowCopy.h"

//package include
#include "HadronicRecoilPFO2016/Ihadrecoil.h"
#include "HadronicRecoilPFO2016/softkiller.h"

class hadrecoil : public asg::AsgTool //, virtual public ::Ihadrecoil
//class hadrecoil
  {
  //ASG_TOOL_CLASS( hadrecoil, Ihadrecoil )

   
    ///////////////////////////////////////////////////////////////////
    // Public methods:
    ///////////////////////////////////////////////////////////////////
  
  public:

    // Copy constructor:

    /// Constructor with parameters:
    hadrecoil(const std::string& name, float Drcone = 0.2, float MinDistCone = 0.4, float neutralPtCut=0.) ;
    /// Destructor:
    ~hadrecoil();

    // Athena algtool's Hooks
    StatusCode  initialize();
    StatusCode  finalize();

    int ncones;
    int nCharged;
    int nNeutral;
    TLorentzVector getHadRecoilCaloCluster(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEM=false);
    TLorentzVector getHadRecoilPFO(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEMCentral=false, bool doEMForward=false);
    TLorentzVector getHadRecoilPFONeutral(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEMCentral=false, bool doEMForward=false);
    TLorentzVector getHadRecoilPFOCharged(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons); 
    TLorentzVector getHadRecoilPFOVoronoi(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEMCentral=false, bool doEMForward=false);
    TLorentzVector getHadRecoilPFOVoronoiSpreading(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEMCentral=false, bool doEMForward=false);
    TLorentzVector getHadRecoilPFOSKVoronoi(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEMCentral=false, bool doEMForward=false);

    double getSumET_CaloCluster(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons, bool doEM=false);
    double getSumET_PFO(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons=0, bool doEMCentral=false, bool doEMForward=false);
    double getSumET_PFONeutral(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons=0, bool doEMCentral=false, bool doEMForward=false);
    double getSumET_PFOCharged(xAOD::TEvent& m_event, xAOD::IParticleContainer* leptons=0);
    ///////////////////////////////////////////////////////////////////
    // Const methods:
    ///////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////
    // Non-const methods:
    ///////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////
    // Private data:
    ///////////////////////////////////////////////////////////////////
  private:
    float deltaPhi(float phi1, float phi2) {
      float dphi = std::fabs(phi1-phi2);
      if (dphi > TMath::Pi()) dphi = 2*TMath::Pi() - dphi;
      return dphi;
    }
    
    float deltaR(float eta1, float phi1, float eta2, float phi2) {
      float deta = eta1-eta2;	 
      float dphi = hadrecoil::deltaPhi(phi1,phi2);		
      return sqrt(deta*deta+dphi*dphi);	 
    }	
    float deltaPhi(const xAOD::IParticle& j1, const xAOD::IParticle& j2) { 
      return TVector2::Phi_mpi_pi(j1.phi()-j2.phi());
    }
    
    float drCone, minDistCone ; // Size of replacement cone & min distance to recoil and lepton
    float m_neutralPtCut;
    
    TRandom3 *hole;
    xAOD::PFOContainer* neutralPFOs;
    xAOD::PFOContainer* chargedPFOs;
    xAOD::PFOContainer* chargedSigPFOs; // charged PFOs with associated track from PV
    xAOD::PFOAuxContainer* neutralPFOsAux;
    xAOD::PFOAuxContainer* chargedPFOsAux;
    xAOD::PFOAuxContainer* chargedSigPFOsAux; // charged PFOs with associated track from PV
    xAOD::CaloClusterContainer *topoclusters;
    xAOD::CaloClusterAuxContainer *topoclustersAux;
    double sumET_PFO( xAOD::IParticleContainer* NeutralPFOs, xAOD::IParticleContainer* ChargedPFOs, xAOD::IParticleContainer* leptons=0);
    double sumETCorr_PFO( xAOD::IParticleContainer *container, xAOD::IParticleContainer* NeutralPFOs, xAOD::IParticleContainer* ChargedPFOs);

    TLorentzVector sumClusters_PFO(xAOD::IParticleContainer* NeutralPFOs, xAOD::IParticleContainer* ChargedPFOs);
    TLorentzVector hadrecoil_PFO(xAOD::IParticleContainer *container, xAOD::IParticleContainer* NeutralPFOs, xAOD::IParticleContainer* ChargedPFOs);
    TLorentzVector hadrecoilCorr_PFO( xAOD::IParticleContainer *container, xAOD::IParticleContainer* NeutralPFOs, xAOD::IParticleContainer* ChargedPFOs);
    void getChargedGoodPFOContainer(xAOD::TEvent& m_event);
    void getChargedPFOContainer(xAOD::TEvent& m_event);
    void getNeutralPFOContainer(xAOD::TEvent& m_event, bool doEMCentral=false, bool doEMForward=false);
    void getCaloClusterContainer(xAOD::TEvent& m_event, bool doEM);

    void MakeVoronoiNeutralPFOs(xAOD::PFOContainer* NeutralPFOs, xAOD::PFOContainer* ChargedPFOs, xAOD::PFOContainer &newPFOContainer, bool doSpreading);
    void applyVoronoiSpreading(xAOD::PFOContainer &newPFOContainer, std::vector< fastjet::PseudoJet > &constituents);
    void applyVoronoiSubtraction(xAOD::PFOContainer &newPFOContainer,std::vector< fastjet::PseudoJet > &constituents, float rho);
    StatusCode clean();
    float getRho(std::vector< fastjet::PseudoJet > inputConst);
    
    std::vector<double> getRnd(TLorentzVector HR, xAOD::IParticleContainer *container);
    const xAOD::Vertex *pv;
    std::vector<double> rnd;
  
    CP::WeightPFOTool m_weightPFOTool;   
    //   CP::WeightPFOTool* m_weightPFOTool;    /// Retrieval tool
    //double sumET(xAOD::TEvent& m_event);
    /// Default constructor:
    hadrecoil();

  };

#endif //> !HADRECOIL_H
