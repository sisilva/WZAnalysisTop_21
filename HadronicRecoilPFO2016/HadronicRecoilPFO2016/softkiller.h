//Softkiller for hadronic recoil package

//authors : 	jennifer.roloff@cern.ch
//		fabrice.balli@cern.ch
#ifndef softkiller_h
#define softkiller_h
#include "fastjet/PseudoJet.hh"
#include "fastjet/JetDefinition.hh"
#include "fastjet/Selector.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/contrib/SoftKiller.hh"
//should be already included
//#include "fastjet/RectangularGrid.hh"

#include "xAODPFlow/PFOContainer.h"
#include "xAODTracking/VertexContainer.h"
#include <string>
#include <iostream>
#include <vector>
#include "HadronicRecoilPFO2016/Isoftkiller.h"
//////////////////////////////////////////////////
//Allows you to store info about PFO type (Ch. hard scatter, ch. pileup, neutral) in the PseudoJet
/////////////////////////////////////////////////
class MyPFOInfo : public fastjet::PseudoJet::UserInfoBase{
  public:
    MyPFOInfo(const std::string pfoType) : myType(pfoType){}
    std::string getType() const{ return myType;}
  protected:
    std::string myType;
};
class softkiller{
  public :
    softkiller();
    ~softkiller();
///////////////////////////////////////////////////////////////////////////
//Creates PseudoJet collection of PFO
//Each pseudojet has a tag identifiying what type of PFO (HSCh, PUCh, Ne)
//////////////////////////////////////////////////////////////////////////
std::vector<fastjet::PseudoJet> createPflowClusters(const xAOD::PFOContainer *m_pflow_charged, const xAOD::PFOContainer *m_pflow_neutral);

//////////////////////////////////////////////////////////////////////////////// 
//Creates a new cluster collection with Voronoi subtraction applied to neutral
//Note that all PFO are used in calculating the Voronoi area
////////////////////////////////////////////////////////////////////////////////
std::vector<fastjet::PseudoJet>  createVoronoiClusters(std::vector<fastjet::PseudoJet> clusters, double rho, double jetR);

////////////////////////////////////////////////////////////////////////
//Creates a new PFO collection with HS charged PFO and neutral PFO
//Neutrals have both Voronoi and SK applied
///////////////////////////////////////////////////////////////////////
std::vector<fastjet::PseudoJet> createSKClusters(std::vector<fastjet::PseudoJet> PFO, double gridSpacing, double rapmax);
std::vector<fastjet::PseudoJet> createNeutralSKClusters(std::vector<fastjet::PseudoJet> PFO, double gridSpacing, double rapmax);

///////////////////////////////////////////////////////////////////////////////////////////////
//Applies SK to only neutral particles and eliminates charged PU from the clusters collection
///////////////////////////////////////////////////////////////////////////////////////////////
std::vector<fastjet::PseudoJet> applyThreshold(std::vector<fastjet::PseudoJet> clusters, double threshold);
};
#endif // softkiller_h
