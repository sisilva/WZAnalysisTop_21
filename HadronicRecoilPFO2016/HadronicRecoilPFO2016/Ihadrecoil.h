// Ihadrecoil.h

//////////////////////////////////////////////////////////
/// class Ihadrecoil
/// 
/// Interface for the Had recoil tool
///
//////////////////////////////////////////////////////////

#ifndef IHADRECOIL_H
#define IHADRECOIL_H

#include "AsgTools/IAsgTool.h"


class Ihadrecoil : virtual public asg::IAsgTool {

  ASG_TOOL_INTERFACE( Ihadrecoil )

public:

  /// Initialize the tool.
  virtual StatusCode initializeTool(const std::string& name) = 0;


};

#endif
