// Ihadrecoil.h

//////////////////////////////////////////////////////////
/// class Ihadrecoil
/// 
/// Interface for the Had recoil tool
///
//////////////////////////////////////////////////////////

#ifndef ISOFTKILLER_H
#define ISOFTKILLER_H

#include "AsgTools/IAsgTool.h"


class Isoftkiller : virtual public asg::IAsgTool {

  ASG_TOOL_INTERFACE( Isoftkiller )

public:

  /// Initialize the tool.
  virtual StatusCode initializeTool(const std::string& name) = 0;


};

#endif
